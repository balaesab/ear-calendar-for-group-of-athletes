# Calendar for Group of Athletes

Aplikace je určena pro skupinu atletů. Atleti v této skupině se kromě tréninků účastní také běžeckých závodů. Naše
aplikace se bude zabývat výhradně těmito závody. Atleti by chtěli mít přehled o svých nadcházejících a plánovaných
závodech a také o tom, kdo se jich účastní.

Závody budou atleti moci sledovat v kalendáři, který by měl zaručit dostatečnou přehlednost. Jedná se o hlavní
funkcionalitu.

Budou zde dva typy uživatelů: atleti a trenéři. Závody budou přídávány kterýmkoli z nich. U trenérů nebude nutné vést
ročník narození. Atlety bude možné rozřadit do jednotlivých kategorií.

Aplikace také umožňuje zobrazit seznam atletů využívajících aplikaci. Další funkionalitou je navíc možnost pro každého
uživatele prohlížet si seznam závodů ostatních atletů. Když si tedy v seznamu atletů vybere atleta, může se dostat na
stránku jeho profilu a tam kromě jiných osobních informací uvidí, kterých závodů se v historii tento atlet účastnil a
jakých nadcházejících se účastnit hodlá. Vedle toho bude atlet moci zadat do aplikace výsledek závodu, na němž běžel.
Tyto časy si pak může zobrazit v podobě grafu a pozorovat tak své výsledky z jednotlivých závodů.

Závod bude obsahovat informace jako datum, čas a místo. Bude také možné doplnit odkaz na webovou stránku závodu s
bližšími informacemi a poznámku. Aplikace umožní atletům u konkrétního závodu označit, zda se ho budou/nebudou účastnit.
Zároveň bude k dispozici seznam účastníků na dané události.

Aplikace umožňuje vytvářet tzv. seznamy blízkých přátel (každý atlet má svůj seznam). V kalendáři potom budou pro daného
atleta zvýrazněny určité závody, kterých se účastní některý atlet z jeho seznamu. V seznamu účastníků daného závodu
budou zvýraznění ti atleti, kteří se nachází v uživatelově seznamu blízkých přátel.

Počet uživatelů: předpokládáme, že počet uživatelů se bude pohybovat v rozmezí 10-50.

## Funkcionality:

- všichni vidí přehledy závodů ostatních atletů
- atlet může přidat své výsledky ke každému závodu a zobrazit přehled svých závodů a výsledků na těchto závodech
- odkaz na web závodu s informacemi o závodě
- označování zájmu o účast na závodě
- zvýraznění účastníků z uživatelova seznamu blízkých přátel v kalendáři
- závody, kterých se účastní blízcí přátelé uživatele, tento uživatel vidí zvýrazněné

## Podněty k diskuzi zadání:

- seznam místo kalendáře: Pokud aplikace nebude určena i pro vedení tréninků, bude kalendář méně prostorově efektivní (
  pondělí-pátek se závody většinou nekonají, a tak by tyto buňky byly prázdné) než například seznam.

