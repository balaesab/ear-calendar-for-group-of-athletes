DROP TABLE IF EXISTS address_location;
DROP TABLE IF EXISTS app_user cascade;
DROP TABLE IF EXISTS close_friend;
DROP TABLE IF EXISTS competition cascade;
DROP TABLE IF EXISTS gps_location;
DROP TABLE IF EXISTS "group" cascade;
DROP TABLE IF EXISTS group_member;
DROP TABLE IF EXISTS location cascade;
DROP TABLE IF EXISTS participation;
DROP TABLE IF EXISTS race;

CREATE TABLE "app_user"
(
    id         int primary key,
    firstName  varchar not null,
    lastname   varchar not null,
    username   varchar not null unique,
    "password" varchar not null,
    "role"     int     not null
);

CREATE TABLE "competition"
(
    id     int primary key,
    "name" varchar unique not null
);

CREATE TABLE "location"
(
    id         int primary key,
    "name"     varchar(63),
    detailInfo varchar,
    "type"     int not null
);

CREATE TABLE "close_friend"
(
    ownerId  int not null references app_user (id) on update cascade,
    friendId int not null references app_user (id) on update cascade,
    primary key (ownerId, friendId)
);

CREATE TABLE "group"
(
    id      int primary key,
    "name"  varchar not null,
    creator int not null references app_user (id) on update cascade
);

CREATE TABLE "race"
(
    id          int primary key,
    "name"      varchar not null,
    "date"      date    not null,
    "time"      time    not null,
    locationId  int     not null references location (id) on update cascade,
    competition int references competition (id) on update cascade,
    startingFee int     not null,
    note        varchar,
    isRemoved   bool    not null
);

CREATE TABLE participation
(
    id                  int primary key,
    isRealized          boolean not null,
    resultTimeInSeconds double precision,
    participant         int     not null references app_user (id) on update cascade,
    race                int     not null references race (id) on update cascade
);

CREATE TABLE "group_member"
(
    member  int not null references app_user (id) on update cascade,
    "group" int not null references "group" (id) on update cascade,
    primary key (member, "group")
);

CREATE TABLE "gps_location"
(
    id        int primary key,
    latitude  double precision not null,
    longitude double precision not null,
    foreign key (id) references location on update cascade on delete cascade
);

CREATE TABLE "address_location"
(
    id     int primary key,
    city   varchar(128) not null,
    street varchar(128) not null,
    number int          not null,
    foreign key (id) references location on update cascade on delete cascade
);



INSERT INTO "app_user"(id, firstName, lastname, username, "password", "role")
values (1, 'Pepa', 'Novak', 'pepik', '$2a$10$rfa41XK5HdizO6D0gYhnHOegoI22X.DtRP1sOQERBLw1ccXZnaKhG', '1'),
       (2, 'Alena', 'Vesela', 'alca', '$2a$10$rfa41XK5HdizO6D0gYhnHOegoI22X.DtRP1sOQERBLw1ccXZnaKhG', '1'),
       (3, 'Tomas', 'Novy', 'tomik', '$2a$10$rfa41XK5HdizO6D0gYhnHOegoI22X.DtRP1sOQERBLw1ccXZnaKhG', '0'),
       (4, 'Honza', 'Novotny', 'honzik', '$2a$10$rfa41XK5HdizO6D0gYhnHOegoI22X.DtRP1sOQERBLw1ccXZnaKhG', '0'),
       (5, 'Tereza', 'Cerna', 'terka', '$2a$10$rfa41XK5HdizO6D0gYhnHOegoI22X.DtRP1sOQERBLw1ccXZnaKhG', '0'),
       (6, 'Sara', 'Svobodova', 'sara', '$2a$10$rfa41XK5HdizO6D0gYhnHOegoI22X.DtRP1sOQERBLw1ccXZnaKhG', '0');

INSERT INTO "close_friend"(ownerId, friendId)
values (1, 6),
       (2, 5),
       (2, 6),
       (3, 4),
       (3, 5),
       (3, 6),
       (4, 2),
       (4, 3),
       (4, 5),
       (4, 6),
       (5, 1),
       (5, 2),
       (5, 3),
       (5, 4),
       (5, 6);

INSERT INTO "location"(id, "name", detailInfo, "type")
values (1, 'Pardubice', 'Dostanete se tam autobusem č. 211, pořadatelé na sobě budou mít ČERVENÁ trička!', 1),
       (2, 'Rtýně v Podkrkonoší', 'Dostanete se tam autobusem č. 221, pořadatelé na sobě budou mít ŽLUTÁ trička!', 1),
       (3, 'Ratibořice', 'Dostanete se tam autobusem č. 131, pořadatelé na sobě budou mít ZELENÁ trička!', 1),
       (4, 'Brněnská přehrada', 'Dostanete se tam autobusem č. 142, pořadatelé na sobě budou mít ORANŽOVÁ trička', 2),
       (5, 'Strahovský stadion', 'Dostanete se tam autobusem č. 152, pořadatelé na sobě budou mít MODRÁ trička', 2),
       (6, 'SK Aritma', 'Dostanete se tam autobusem č. 162, pořadatelé na sobě budou mít FIALOVÁ trička', 2);

INSERT INTO "competition"(id, "name")
values (1, 'Behy mimo drahu'),
       (2, 'Krajska mistrovstvi');

INSERT INTO "race"(id, "name", "date", "time", locationId, competition, startingFee, note, isRemoved)
values (1, 'Zahajovací závod v tyči AC Pardubice', '2023-1-14', '10:00:00', 1, 1, 50, '', false),
       (2, 'Rtyňské okruhy sídlištěm', '2023-2-23', '15:00:00', 2, 1, 50, '', false),
       (3, 'Běh Ratibořickým údolím', '2023-3-5', '11:00:00', 3, 1, 50,
        'Doběživše dostávají účastníci palačinky se zmrzlinou.', false),
       (4, 'Přebor Prahy v halových vícebojích', '2023-1-22', '10:00:00', 5, 2, 50, '', false),
       (5, 'Přebor Moravskoslezského kraje dorostu, juniorů a dospělých', '2023-2-10', '10:00:00', 4, 2, 50, '',
        false),
       (6, 'Středočeský krajský přebor staršího žactva', '2023-3-15', '11:00:00', 5, 2, 50, '', false),
       (7, 'Prespolni beh v Divoke Sarce', '2023-1-30', '14:00:00', 6, null, 50, '', false);

INSERT INTO "participation"(id, isRealized, resultTimeInSeconds, participant, race)
values (1, false, null, 1, 1),
       (2, false, null, 2, 1),
       (3, false, null, 3, 1),
       (4, false, null, 4, 1),
       (5, false, null, 5, 1),
       (6, false, null, 6, 1),
       (10, false, null, 4, 2),
       (11, false, null, 5, 2),
       (12, false, null, 6, 2);

INSERT INTO "group"(id, "name", creator)
values (1, 'tygri', 1),
       (2, 'gepardi', 2),
       (3, 'zajici', 2);

INSERT INTO "group_member"(member, "group")
values (1, 1),
       (2, 2),
       (3, 1),
       (4, 2),
       (6, 3);

INSERT INTO "address_location"(id, city, street, number)
values (1, 'Pardubice', 'Libčická', 5),
       (2, 'Rtyně v Podkrkonoší', 'Krkonošská', 10),
       (3, 'Havlovice', 'Havlovická', 20);

INSERT INTO "gps_location"(id, latitude, longitude)
values (4, 49.851675, 18.286492),
       (5, 50.0804993, 14.3855583),
       (6, 50.0986132, 14.3402227);


