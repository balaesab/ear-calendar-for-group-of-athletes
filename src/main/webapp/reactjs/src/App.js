import './css/App.css';

import {BrowserRouter as Router, Routes, Route} from "react-router-dom";
import {useState} from "react";
import Login from "./components/Login.js";
import NavigationBar from "./components/NavigationBar";
import Users from "./components/Users";
import Races from "./components/Races";
import RaceDetail from "./components/RaceDetail.js";
import Membership from "./components/Membership";
import GroupDetail from "./components/GroupDetail";

function App() {

    const [token, setToken] = useState();

    if (!token) {
        // return <Login setToken={setToken}/>
    }

    return (
        <Router>
            <Routes>
                <Route path="/users" exact element={<div><NavigationBar/><Users/></div>}/>
                <Route path="/races" exact element={<div><NavigationBar/><Races/></div>}/>
                <Route path="/login" exact element={<Login/>}/>
                <Route path="/raceDetail/:raceId" element={<div><NavigationBar/><RaceDetail/></div>}/>
                <Route path="/membership" element={<div><NavigationBar/><Membership/></div>}/>
                <Route path="/groupDetail/:groupId" element={<div><NavigationBar/><GroupDetail/></div>}/>
                <Route path="/userDetail/:userId" element={<div><NavigationBar/><p>UserDetail</p></div>}/>
            </Routes>
        </Router>
    );
}

export default App;
