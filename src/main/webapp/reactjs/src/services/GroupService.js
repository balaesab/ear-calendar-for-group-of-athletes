import axios from "../axios";

const GROUPS_REST_API_URL = "/rest/groups";

export default class GroupService {
    static getGroups() {
        return axios.get(GROUPS_REST_API_URL);
    }

    static getGroupById(id) {
        return axios.get(GROUPS_REST_API_URL + "/" + id);
    }
}