import axios from "../axios";

const USERS_REST_API_URL = "/rest/users";
const MEMBERSHIP_URL = "/membership";
const SECURITY_CHECK_URL = "/login";

export default class UserService {

    static getOwnMembership() {
        return axios.get(USERS_REST_API_URL + MEMBERSHIP_URL);
    }

    static postCredentials(username, password) {
        const params = new URLSearchParams();       //uses post method
        params.append('username', username);
        params.append('password', password);
        return axios.post(SECURITY_CHECK_URL, params).then(response => response.data.json());
    }

    static getUsers() {
        return axios.get(USERS_REST_API_URL);
    }
}