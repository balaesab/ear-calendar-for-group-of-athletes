import axios from "../axios";

const RACES_REST_API_URL = "/rest/races";
const RACE_DETAIL_REST_API_URL = "/rest/races/1";

export default class RaceService {
    static getRaces() {
        return axios.get(RACES_REST_API_URL);
    }

    static getRaceById(id) {
        return axios.get(RACES_REST_API_URL + "/" + id);
    }

    static getRace() {
        return axios.get(RACE_DETAIL_REST_API_URL);
    }

    static postRace(name, competitionName, competitionId, date, time, location, latitude, longitude, city, street, number, startingFee, note) {
        return axios.post(RACES_REST_API_URL, {
            name,
            competitionName,
            competitionId,
            date,
            time,
            location,
            latitude,
            longitude,
            city,
            street,
            number,
            startingFee,
            note
        });
    }
}
