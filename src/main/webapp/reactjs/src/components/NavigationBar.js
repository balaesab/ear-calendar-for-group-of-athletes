import React, {Component} from "react";
import {Link} from "react-router-dom";
import withRouter from "./withRouter";
import styles from '../css/NavigationBar.module.css'

class NavigationBar extends Component {
    render() {
        return (
            <header>
                {/*<div className={styles.search}>*/}
                {/*    <input type="text" className={styles.searchbar} placeholder="Search..." />*/}
                {/*    <img src="../images/search.png" alt="" />*/}
                {/*</div>*/}

                <Link to={"/users"} className={styles["nav-login"]}>
                    <input type="button" className={styles["menu-btn"]} value="Athletes"/>
                </Link>

                <Link to={"/races"} className={styles["nav-login"]}>
                    <input type="button" className={styles["menu-btn"]} value="Races"/>
                </Link>

                <Link to={"/membership"} className={styles["nav-login"]}>
                    <input type="button" className={styles["menu-btn"]} value="Membership"/>
                </Link>

                <Link to={"/profile"} className={styles["nav-login"]}>
                    <input type="button" className={styles["menu-btn"]} value="Profile"/>
                </Link>

                <Link to={"/login"} className={styles["nav-login"]}>
                    <input type="button" className={styles["menu-btn"]} value="Login"/>
                </Link>
            </header>
        );
    }
}

export default withRouter(NavigationBar);
