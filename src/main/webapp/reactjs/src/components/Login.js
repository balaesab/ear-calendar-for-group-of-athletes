import React from "react";
import tokenLocalStorage from "./tokenLocalStorage";
//import PropTypes from "prop-types";
import UserService from "../services/UserService";
import styles from '../css/Login.module.css';

export default class Login extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            username: '', password: ''
        }
    }

    render() {
        function validateForm(state) {
            return state.username.length > 0 && state.password.length > 0;
        }

        let handleSubmit = async event => {
            console.log("form submitted: " + this.state.username + ", " + this.state.password)
            event.preventDefault();
            const token = await UserService.postCredentials(this.state.username, this.state.password);
            tokenLocalStorage(this.state).setToken(token);
        }

        console.log("state: " + this.state.username + ", " + this.state.password);

        return (
            <section className={styles["login"]}>
                <h1>Login</h1>
                <div className={styles.form}>
                    <form onSubmit={handleSubmit}>
                        <div className={styles["input-container"]}>
                            <label>Username </label>
                            <input autoFocus type="text" name="username-input" required value={this.state.username}
                                   onChange=
                                       {(event) => this.setState({username: event.target.value})}
                            />
                        </div>
                        <div className={styles["input-container"]}>
                            <label>Password </label>
                            <input type="password" name="password-input" required value={this.state.password} onChange=
                                {(event) => this.setState({password: event.target.value})}
                            />
                        </div>
                        <div className={styles["button-container"]}>
                            <input type="submit" className={styles["btn-blue"]} value="Login"
                                   disabled={!validateForm(this.state)}/>
                        </div>
                    </form>
                </div>
            </section>)
    }
}

// Login.propTypes = {
//     setToken: PropTypes.func.isRequired
// };