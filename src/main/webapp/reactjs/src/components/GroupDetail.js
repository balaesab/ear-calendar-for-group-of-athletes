import React, {Component} from "react";
import GroupService from "../services/GroupService";
import withRouter from './withRouter.js';
import {Link} from "react-router-dom";
import styles from '../css/GroupDetail.module.css';

class GroupDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            groupDetail: [],
            isLoading: false
        };
    }

    componentDidMount() {
        this.setState({isLoading: true})
        GroupService.getGroupById(this.props.params.groupId)
            .then((response) => {
                this.setState({
                    groupDetail: response.data,
                    isLoading: false
                })
            })
            .catch(error => {
                    console.log("ERROR: " + error);
                    this.setState({
                        groupDetail: [],
                        isLoading: false
                    });
                }
            );
    }

    render() {
        const {groupDetail: groupData, isLoading} = this.state;

        if (isLoading) return <h1>Loading...</h1>;

        if (!groupData.name) {
            return<h1>No group fulfilling the requirements (id) found.</h1>;
        }

        let htmlCodeWithMembers = [];
        for (let i = 0; i < groupData.membersUsernames.length; i++) {
            htmlCodeWithMembers.push(
                <tr>
                    <td className={styles.member} key={i}>
                        {groupData.membersFirstNames[i]} {groupData.membersLastNames[i]} ({groupData.membersUsernames[i]})
                    </td>
                    <td className={styles["member-detail"]}>
                        <Link to={"/userDetail/" + groupData.membersIds[i]} className={styles["member-link"]}>
                            <input type="button" className={styles["btn-blue"]} value="View details"/>
                        </Link>
                    </td>
                </tr>
            );
        }

        return(
            <section className={styles["group-detail"]}>;
                <h1>Details of the group</h1>
                <div key="2" className={styles["group-details-container"]}>
                    <div className={styles.card}>
                        <div className={styles.content}>
                            <div className={styles.title}>{groupData.name}</div>
                            <div className={styles.creator}>
                                <Link to={"/userDetail/" + groupData.creatorId} className={styles["creator-link"]}>
                                    <input type="button" className={styles["btn-blue"]} value={groupData.creatorUsername}/>
                                </Link>
                            </div>
                            <div className={styles.note}>Poznamka: {groupData.note}</div>
                        </div>
                    </div>
                    <div key="2" className={styles.card}>
                        <div className={styles.title}>Members</div>
                        <table>
                            <tbody>
                            <tr>
                                <th>User</th>
                                <th>null</th>
                            </tr>
                            {htmlCodeWithMembers}
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        )
    }
}
export default withRouter(GroupDetail);