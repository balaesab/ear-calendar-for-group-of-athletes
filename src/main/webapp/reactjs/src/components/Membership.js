import React, {Component} from "react";
import UserService from "../services/UserService";
import styles from '../css/Membership.module.css'

export default class Membership extends Component {
    constructor(props) {
        super(props);
        this.state = {
            groups: [],
            isLoading: false
        };
    }

    componentDidMount() {
        this.state.isLoading = true;
        UserService.getOwnMembership()
            .then((response) => {
                this.setState({
                    groups: response.data,
                    isLoading: false
                })
            })
            .catch(error => {
                    console.log("ERROR: " + error);
                    this.setState({
                        groups: [],
                        isLoading: false
                    });
                }
            );
    }

    render() {
        const {groups: groupsData, isLoading} = this.state;

        if (isLoading) return <h1>Loading...</h1>;

        if (groupsData.length === 0) {
            return<p>No groups.</p>;
        }

        let htmlCodeWithGroups = [];
        for (let i = 0; i < groupsData.ids.length; i++) {
            htmlCodeWithGroups.push(
                <div key={groupsData.ids[i]} className={styles.line}>
                    <div className={styles.content}>
                        <div className={styles.title}>{groupsData.names[i]}</div>
                        <div className={styles.creator}>Creator:</div>
                        <div className={styles["creator-username"]}>{groupsData.creatorsUsernames[i]}</div>
                        <div className={styles.members}>Members:</div>
                        <div className={styles["number-of-members"]}>{groupsData.numbersOfMembers[i]}</div>
                    </div>
                </div>
            );
        }

        return (<section className={styles["group-list"]}>
            <h1>Groups</h1>
            <div className={styles.container}>
                {htmlCodeWithGroups}
            </div>
        </section>)
    }
}