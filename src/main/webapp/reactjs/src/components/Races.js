import React, {Component} from "react";
import RaceService from "../services/RaceService";
import {Link} from "react-router-dom";
import styles from '../css/Races.module.css'
import Modal from "./Modal";

export default class Races extends Component {
    constructor(props) {
        super(props);
        this.state = {
            races: [],
            isLoading: false
        };
    }

    componentDidMount() {
        this.setState({isLoading: true})
        RaceService.getRaces()
            .then((response) => {
                console.log(response);
                this.setState({
                    races: response.data,
                    isLoading: false
                })
            })
            .catch(error => {
                    console.log("ERROR: " + error);
                    this.setState({
                        races: [],
                        isLoading: false
                    });
                }
            );
    }

    render() {
        const {races, isLoading} = this.state;

        if (!this.state.races.racesIds) {
            return;
        }

        if (isLoading) return <h1>Loading...</h1>;

        let htmlCodeWithRaces = [];
        for (let i = 0; i < races.racesIds.length; i++) {
            htmlCodeWithRaces.push(
                <div key={races.racesIds[i]} className={styles.card} id={styles[races.highlightingValues[i]]}>
                    <div className={styles.content}>
                        <div className={styles.title}>{races.racesNames[i]}</div>
                        <div className={styles.competition}>{races.competitionsNames[i]}</div>
                        <div className={styles.row}>
                            <div className={styles.date}>{races.racesDates[i]}</div>
                            <div className={styles.time}>{races.racesTimes[i]}</div>
                        </div>
                        <div className={styles.info}>{races.raceLocationsNames[i]}</div>
                        <div className={styles["view-btn"]}>
                            <Link to={"/raceDetail/" + races.racesIds[i]} className={styles["race-link"]}>
                                <input type="button" className={styles["btn-blue"]} value="View details"/>
                            </Link>
                        </div>
                    </div>
                </div>
            );
        }

        return (<section className={styles["race-list"]}>
            <div className={styles["header-row"]}>
                <h1>Races</h1>
                {/*<input type="button" className=styles["header-btn"] value="Add new race"/>*/}
                <Modal className={styles["header-btn"]}></Modal>
            </div>
            <div className={styles.container}>
                {htmlCodeWithRaces}
            </div>
        </section>)
    }
}
