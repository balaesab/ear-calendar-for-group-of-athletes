import React, {Component} from "react";
import RaceService from "../services/RaceService";
import withRouter from './withRouter.js';
import styles from '../css/RaceDetail.module.css';

class RaceDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            raceDetail: [],
            isLoading: false
        };
    }

    componentDidMount() {
        this.setState({isLoading: true})
        RaceService.getRaceById(this.props.params.raceId)
            .then((response) => {
                this.setState({
                    raceDetail: response.data,
                    isLoading: false
                })
            })
            .catch(error => {
                    console.log("ERROR: " + error);
                    this.setState({
                        raceDetail: [],
                        isLoading: false
                    });
                }
            );
    }

    render() {
        const {raceDetail, isLoading} = this.state;

        if (isLoading) return <h1>Loading...</h1>;

        if (!raceDetail.participatingUsernames) {
            return <p>No participants.</p>;
        }

        let htmlCodeWithCloseFriends = [];
        for (let i = 0; i < raceDetail.participatingUsernames.length; i++) {
            htmlCodeWithCloseFriends.push(
                <tr>
                    <td className={styles["td-name"]} key={i}>
                        {raceDetail.participatingFirstNames[i]} {raceDetail.participatingLastNames[i]} ({raceDetail.participatingUsernames[i]})
                    </td>
                    <td className={styles["td-time"]}>null</td>
                </tr>
            );
        }

        return (
            <section className={styles["race-detail"]}>
                <div className={styles["header-row"]}>
                    <h1>Details of the race</h1>
                    <input type="button" className={styles["header-btn"]} value="Participate"/>
                </div>
                <div className={styles.container}>
                    <div key="1" className={styles.card}>
                        <div className={styles.content}>
                            <div className={styles.title}>{raceDetail.name}</div>
                            <div className={styles.competition}>{raceDetail.competitionName}</div>
                            <div className={styles.row}>
                                <div className={styles.date}>{raceDetail.date}</div>
                                <div className={styles.time}>{raceDetail.time}</div>
                            </div>
                            <div className={styles.row}>
                                <div className={styles.location}>{raceDetail.locationName}</div>
                                <div className={styles.startingFee}>{raceDetail.startingFee} CZK</div>
                            </div>
                            <div className={styles.note}>Poznamka: {raceDetail.note}</div>
                        </div>
                    </div>
                    <div key="2" className={styles.card}>
                        <div className={styles.title}>Participants</div>
                        <table>
                            <tbody>
                            <tr>
                                <th className={styles["th-name"]}>Name</th>
                                <th className={styles["th-time"]}>Time</th>
                            </tr>
                            {htmlCodeWithCloseFriends}
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        )
    }
}

export default withRouter(RaceDetail);