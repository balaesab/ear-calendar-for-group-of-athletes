import React, {useEffect, useState} from "react";
import styles from "../css/Modal.module.css";
import styles2 from "../css/Login.module.css";
import RaceService from "../services/RaceService";
import axios from "../axios";

export default function Modal() {

    const [modal, setModal] = useState(false);
    const [name, setName] = useState('')
    const [competitionName, setCompetitionName] = useState('')
    const [date, setDate] = useState('')
    const [time, setTime] = useState('')
    const [city, setCity] = useState('')
    const [street, setStreet] = useState('')
    const [number, setNumber] = useState('')
    const [latitude, setLatitude] = useState('')
    const [longitude, setLongitude] = useState('')
    const [show, setShow] = useState(1)

    const toggleModal = () => {
        setModal(!modal);
    }

    const handleShow = (show) => {
        setShow(show);
    }


    const postData = async (e) => {
        e.preventDefault();
        console.log(name);
        const headers = {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,PATCH,OPTIONS'
        };

        await axios.post(
            '/rest/races',
            {
                name: name
            },
            {headers}
        )
    }

    // const params = new URLSearchParams();       //uses post method
    //     params.append('username', name);
    //     return axios.post('/rest/races', params).then(response => response.data.json());
    //     // axios.post("/rest/races", {
    //     //     name
    //     // })
    //     //     .then(res => console.log("Posting data", res))
    //     //     .catch(err => console.log(err));
    //     // RaceService.postRace(name, competitionName, date, time, location, null, null, null, null, null, null, null, null)
    // }

    return (
        <>
            <button onClick={toggleModal} className={styles["header-btn"]}>Add new race</button>

            {modal && (
                <div className={styles.modal}>
                    <div className={styles.overlay} onClick={toggleModal}></div>
                    <div className={styles["modal-content"]}>
                        <h1>Add new race</h1>
                        <form onSubmit={postData}>
                            <div className={styles.container}>
                                <div className="row">
                                    <div className={styles.left}>
                                        <div className={styles2["input-container"]}>
                                            <label>Name </label>
                                            <input autoFocus type="text" value={name}
                                                   onChange={(e) => setName(e.target.value)}
                                                   name="username-input"/>
                                        </div>
                                        <div className={styles2["input-container"]}>
                                            <label>Competition </label>
                                            <input type="text" value={competitionName}
                                                   onChange={(e) => setCompetitionName(e.target.value)}
                                                   name="password-input"/>
                                        </div>
                                        <div className={styles2["input-container"]}>
                                            <label>Date </label>
                                            <input type="date" value={date} onChange={(e) => setDate(e.target.value)}
                                                   name=""/>
                                        </div>
                                        <div className={styles2["input-container"]}>
                                            <label>Time </label>
                                            <input type="time" value={time} onChange={(e) => setTime(e.target.value)}
                                                   name=""/>
                                        </div>
                                    </div>
                                    <div className={styles.right}>
                                        <div className="location-radio-btns">
                                            <label>Location </label><br/>
                                            <input type="radio" name="address" value="address" checked={show === 1}
                                                   onChange={(e) => handleShow(1)}/>
                                            <label htmlFor="address">Address location</label><br/>

                                            <input type="radio" name="gps" value="gps" checked={show === 2}
                                                   onChange={(e) => handleShow(2)}/>
                                            <label htmlFor="gps">GPS location</label>
                                        </div>

                                        {show === 1 && (
                                            <div>
                                                <div className={styles2["input-container"]}>
                                                    <label>City </label>
                                                    <input type="text" value={city}
                                                           onChange={(e) => setCity(e.target.value)} name=""/>
                                                </div>

                                                <div className={styles2["input-container"]}>
                                                    <label>Street </label>
                                                    <input type="text" value={street}
                                                           onChange={(e) => setStreet(e.target.value)} name=""/>
                                                </div>

                                                <div className={styles2["input-container"]}>
                                                    <label>Number </label>
                                                    <input type="text" value={number}
                                                           onChange={(e) => setNumber(e.target.value)} name=""/>
                                                </div>
                                            </div>
                                        )}
                                        {show === 2 && (
                                            <div>
                                                <div className={styles2["input-container"]}>
                                                    <label>Latitude </label>
                                                    <input type="text" value={latitude}
                                                           onChange={(e) => setLatitude(e.target.value)} name=""/>
                                                </div>

                                                <div className={styles2["input-container"]}>
                                                    <label>Longitude </label>
                                                    <input type="text" value={longitude}
                                                           onChange={(e) => setLongitude(e.target.value)} name=""/>
                                                </div>
                                            </div>
                                        )}
                                    </div>
                                </div>
                                <div className={styles.col}>
                                    <button className={styles["btn-modal-cancel"]} onClick={toggleModal}>Cancel</button>
                                    <input type="submit" className={styles["btn-modal-add"]} value="Add race"/>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            )}
        </>
    )
}