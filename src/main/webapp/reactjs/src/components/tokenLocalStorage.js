export default function tokenLocalStorage(state) {
    const getToken = () => {
        const tokenString = localStorage.getItem('token');
        const userToken = JSON.parse(tokenString);
        return userToken?.token
    };

    // const [token, setToken] = useState(getToken());
    const saveToken = userToken => {
        localStorage.setItem('token', JSON.stringify(userToken));
        state.token = userToken.token;
    };

    return {
        setToken: saveToken,
        token: getToken()
    }
}