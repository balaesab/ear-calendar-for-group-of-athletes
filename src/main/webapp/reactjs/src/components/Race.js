import React, {Component} from "react";
import RaceService from "../services/RaceService";

export default class Race extends Component {
    constructor(props) {
        super(props);

        this.state = {
            product:
                {
                    name: "Beh 10 km",
                    competition: "Mistrovstvi sveta",
                    date: '25/01/2023',
                    time: '10:00',
                    location: "Divoka Sarka",
                    closeFriends: [{name: "alena"}, {name: "honza"}]
                }
        }
    }

    componentDidMount() {
        RaceService.getRaceById(1).then((response) =>
            this.setState({product: response.data})
        );
    }

    render() {
        const {products} = this.state;
        let p;
        return (
            <div key={product.id} className="card">
                <div className="content">
                    <div className="title">{product.name}</div>
                    <div className="competition">{product.competition}</div>
                    <div className="row">
                        <div className="date">{product.date}</div>
                        <div className="time">{product.time}</div>
                    </div>
                    <div className="time">{product.location}</div>
                    <div className="friends">Friends:</div>
                </div>
            </div>
        )
    }
}