import React, {Component} from "react";
import UserService from "../services/UserService";
import styles from "../css/Users.module.css";

export default class Races extends Component {

    constructor(props) {
        super(props);
        this.state = {
            users: [],
            isLoading: false,
        };
    }

    componentDidMount() {
        this.setState({isLoading: true})
        UserService.getUsers()
            .then((response) => {
                console.log(response);
                console.log(response.data.length)
                this.setState({
                    users: response.data,
                    isLoading: false
                })
            })
            .catch(error => {
                    console.log("ERROR: " + error);
                    this.setState({
                        users: [],
                        isLoading: false
                    });
                }
            );
    }

    render() {
        const {users, isLoading} = this.state;

        if (!this.state.users) {
            return;
        }

        if (isLoading) return <h1>Loading...</h1>;

        let htmlCodeWithUsers = [];

        for (let i = 0; i < users.length; i++) {
            htmlCodeWithUsers.push(
                <div className={styles["user-card"]}>
                    <div className="info">
                        <div className={styles.name}>{users[i].firstName} {users[i].lastName}</div>
                        <div className={styles.username}>{users[i].username}</div>
                        <div className={styles.role}>{users[i].role}</div>
                    </div>

                    <div className={styles["button-container"]}>
                        <input type="button" className={styles["btn-blue"]} value="View profile"/>
                        <input type="button" className={styles["btn-blue"]} value="Add to favourites"/>
                    </div>
                </div>
            )
        }

        return (
            <section className={styles["user-list"]}>
                <div className={styles["header-row"]}>
                    <h1>Athletes</h1>
                </div>
                <div className="container">
                    {htmlCodeWithUsers}
                </div>
            </section>
        )
    }
}