package cz.cvut.kbss.ear.calendarforgroupofathletes.security;

//base from e-shop

import cz.cvut.kbss.ear.calendarforgroupofathletes.model.User;
import cz.cvut.kbss.ear.calendarforgroupofathletes.security.model.AuthenticationToken;
import cz.cvut.kbss.ear.calendarforgroupofathletes.security.model.EarUserPrincipal;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;

public class SecurityUtils {

    /**
     * Gets the currently authenticated user.
     *
     * @return Current user
     */
    public static User getCurrentUser() {
        EarUserPrincipal currentEarUserPrincipal = getCurrentUserDetails();
        return currentEarUserPrincipal == null ? null : currentEarUserPrincipal.getUser();
    }

    /**
     * Gets details of the currently authenticated user.
     *
     * @return Currently authenticated user details or null, if no one is currently authenticated
     */
    public static EarUserPrincipal getCurrentUserDetails() {
        final SecurityContext context = SecurityContextHolder.getContext();
        if (context.getAuthentication() != null && context.getAuthentication().getDetails() instanceof EarUserPrincipal) {
            return (EarUserPrincipal) context.getAuthentication().getDetails();
        } else {
            return null;
        }
    }

    /**
     * Creates an authentication token based on the specified user details and sets it to the current thread's security
     * context.
     *
     * @param earUserPrincipal Details of the user to set as current
     * @return The generated authentication token
     */
    public static AuthenticationToken setCurrentUser(EarUserPrincipal earUserPrincipal) {
        final AuthenticationToken token = new AuthenticationToken(earUserPrincipal.getAuthorities(), earUserPrincipal);
        token.setAuthenticated(true);

        final SecurityContext context = new SecurityContextImpl();
        context.setAuthentication(token);
        SecurityContextHolder.setContext(context);
        return token;
    }

    /**
     * Checks whether the current authentication token represents an anonymous user.
     *
     * @return Whether current authentication is anonymous
     */
    public static boolean isAuthenticatedAnonymously() {
        return getCurrentUserDetails() == null;
    }
}
