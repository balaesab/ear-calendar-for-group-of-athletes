package cz.cvut.kbss.ear.calendarforgroupofathletes.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.time.LocalTime;

@Entity
@Table(name = "participation")
@NamedQueries({
        @NamedQuery(name = "Participation.findByRaceAndParticipant", query = "SELECT p FROM Participation p " +
                "WHERE p.race = :raceId AND p.participant = :participantId")
})
public class Participation extends AbstractEntity {

    private boolean isRealized;
    private double resultTimeInSeconds;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "participant")
    private User participant;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "race")
    private Race race;

    public Participation() {
    }

    public Participation(User participant, Race race) {
        super();
        this.participant = participant;
        this.race = race;
    }

    public boolean isRealized() {
        return isRealized;
    }

    public void setRealized(boolean realized) {
        isRealized = realized;
    }

    public double getResultTimeInSeconds() {
        return resultTimeInSeconds;
    }

    public void setResultTimeInSeconds(double resultTimeInSeconds) {
        this.resultTimeInSeconds = resultTimeInSeconds;
    }

    public User getParticipant() {
        return participant;
    }

    public void setParticipant(User participant) {
        this.participant = participant;
    }

    public Race getRace() {
        return race;
    }

    public void setRace(Race race) {
        this.race = race;
    }
}
