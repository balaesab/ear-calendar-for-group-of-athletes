package cz.cvut.kbss.ear.calendarforgroupofathletes.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "gps_location")
@DiscriminatorValue("2")
public class GPSLocation extends Location {

    private static int DEFAULT_ZOOM = 18;
    private static String[] linkTemplate = {"https://mapy.cz/zakladni?x=", "&y=", "&z=" + DEFAULT_ZOOM};

    private double latitude;
    private double longitude;

    @Override
    public String[] getLinkParts() {
        return new String[]{linkTemplate[0], Double.toString(longitude), linkTemplate[1], Double.toString(latitude), linkTemplate[2]};
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
