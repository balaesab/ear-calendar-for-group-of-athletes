package cz.cvut.kbss.ear.calendarforgroupofathletes.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "address_location")
@DiscriminatorValue("1")
public class AddressLocation extends Location {

    private static String[] linkTemplate = {"https://mapy.cz/zakladni?q=", "%20", "%20"};

    private String city;
    private String street;
    private int number;

    @Override
    public String[] getLinkParts() {
        return new String[]{linkTemplate[0], street, linkTemplate[1], Integer.toString(number), linkTemplate[2], city};
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
