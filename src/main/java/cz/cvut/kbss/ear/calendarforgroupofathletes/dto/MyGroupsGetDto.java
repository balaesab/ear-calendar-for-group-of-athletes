package cz.cvut.kbss.ear.calendarforgroupofathletes.dto;

import java.util.List;

public class MyGroupsGetDto {

    private List<Integer> ids;
    private List<String> names;
    private List<Integer> creatorsIds;
    private List<String> creatorsUsernames;
    private List<Integer> numbersOfMembers;

    public List<Integer> getIds() {
        return ids;
    }

    public void setIds(List<Integer> ids) {
        this.ids = ids;
    }

    public List<String> getNames() {
        return names;
    }

    public void setNames(List<String> names) {
        this.names = names;
    }

    public List<Integer> getCreatorsIds() {
        return creatorsIds;
    }

    public void setCreatorsIds(List<Integer> creatorsIds) {
        this.creatorsIds = creatorsIds;
    }

    public List<String> getCreatorsUsernames() {
        return creatorsUsernames;
    }

    public void setCreatorsUsernames(List<String> creatorsUsernames) {
        this.creatorsUsernames = creatorsUsernames;
    }

    public List<Integer> getNumbersOfMembers() {
        return numbersOfMembers;
    }

    public void setNumbersOfMembers(List<Integer> numbersOfMembers) {
        this.numbersOfMembers = numbersOfMembers;
    }
}
