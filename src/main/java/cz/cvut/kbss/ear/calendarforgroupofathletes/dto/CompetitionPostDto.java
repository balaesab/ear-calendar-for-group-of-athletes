package cz.cvut.kbss.ear.calendarforgroupofathletes.dto;

import java.util.List;

public class CompetitionPostDto {

    private String name;
    private List<Integer> racesIds;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Integer> getRacesIds() {
        return racesIds;
    }

    public void setRacesIds(List<Integer> racesIds) {
        this.racesIds = racesIds;
    }
}
