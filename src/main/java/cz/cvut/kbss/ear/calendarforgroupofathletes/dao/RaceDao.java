package cz.cvut.kbss.ear.calendarforgroupofathletes.dao;

import cz.cvut.kbss.ear.calendarforgroupofathletes.model.Race;
import cz.cvut.kbss.ear.calendarforgroupofathletes.model.User;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public class RaceDao extends BaseDao<Race> {
    protected RaceDao() {
        super(Race.class);
    }

    public List<Race> findSimilarByName(String name) {
        return em.createNamedQuery("Race.findSimilarByName", Race.class).setParameter("name", name).getResultList();
    }

    public List<Race> findByDateRange(Date from, Date to) {
        return em.createNamedQuery("Race.findByDateAndTimeRange", Race.class)
                .setParameter("from", from).setParameter("to", to).getResultList();
    }
}
