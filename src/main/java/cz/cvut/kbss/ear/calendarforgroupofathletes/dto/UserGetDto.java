package cz.cvut.kbss.ear.calendarforgroupofathletes.dto;

import java.time.LocalDate;
import java.util.List;

public class UserGetDto {
    private String username;
    private String firstName;
    private String lastName;
    private int roleOrdinal;

    private List<Integer> attendedRacesIds;
    private List<String> attendedRacesNames;
    private List<LocalDate> attendedRacesDates;
    private List<Integer> attendedRacesCompetitionsIds;
    private List<String> attendedRacesCompetitionsNames;
    private List<Double> attendedRacesTimes;    //nulls if not own profile

    private List<Integer> groupsIds;
    private List<String> groupsNames;

    private List<Integer> closeFriendsIds;
    private List<String> closeFriendsUsernames;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getRoleOrdinal() {
        return roleOrdinal;
    }

    public void setRoleOrdinal(int roleOrdinal) {
        this.roleOrdinal = roleOrdinal;
    }

    public List<Integer> getAttendedRacesIds() {
        return attendedRacesIds;
    }

    public void setAttendedRacesIds(List<Integer> attendedRacesIds) {
        this.attendedRacesIds = attendedRacesIds;
    }

    public List<String> getAttendedRacesNames() {
        return attendedRacesNames;
    }

    public void setAttendedRacesNames(List<String> attendedRacesNames) {
        this.attendedRacesNames = attendedRacesNames;
    }

    public List<LocalDate> getAttendedRacesDates() {
        return attendedRacesDates;
    }

    public void setAttendedRacesDates(List<LocalDate> attendedRacesDates) {
        this.attendedRacesDates = attendedRacesDates;
    }

    public List<Integer> getAttendedRacesCompetitionsIds() {
        return attendedRacesCompetitionsIds;
    }

    public void setAttendedRacesCompetitionsIds(List<Integer> attendedRacesCompetitionsIds) {
        this.attendedRacesCompetitionsIds = attendedRacesCompetitionsIds;
    }

    public List<String> getAttendedRacesCompetitionsNames() {
        return attendedRacesCompetitionsNames;
    }

    public void setAttendedRacesCompetitionsNames(List<String> attendedRacesCompetitionsNames) {
        this.attendedRacesCompetitionsNames = attendedRacesCompetitionsNames;
    }

    public List<Double> getAttendedRacesTimes() {
        return attendedRacesTimes;
    }

    public void setAttendedRacesTimes(List<Double> attendedRacesTimes) {
        this.attendedRacesTimes = attendedRacesTimes;
    }

    public List<Integer> getGroupsIds() {
        return groupsIds;
    }

    public void setGroupsIds(List<Integer> groupsIds) {
        this.groupsIds = groupsIds;
    }

    public List<String> getGroupsNames() {
        return groupsNames;
    }

    public void setGroupsNames(List<String> groupsNames) {
        this.groupsNames = groupsNames;
    }

    public List<Integer> getCloseFriendsIds() {
        return closeFriendsIds;
    }

    public void setCloseFriendsIds(List<Integer> closeFriendsIds) {
        this.closeFriendsIds = closeFriendsIds;
    }

    public List<String> getCloseFriendsUsernames() {
        return closeFriendsUsernames;
    }

    public void setCloseFriendsUsernames(List<String> closeFriendsUsernames) {
        this.closeFriendsUsernames = closeFriendsUsernames;
    }
}
