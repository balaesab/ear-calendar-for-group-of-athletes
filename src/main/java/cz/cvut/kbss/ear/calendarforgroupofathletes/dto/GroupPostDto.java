package cz.cvut.kbss.ear.calendarforgroupofathletes.dto;

public class GroupPostDto {

    private String name;
    private Integer creatorId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Integer creatorId) {
        this.creatorId = creatorId;
    }
}
