package cz.cvut.kbss.ear.calendarforgroupofathletes.dto;

import cz.cvut.kbss.ear.calendarforgroupofathletes.model.Role;

public class UserPostDto {

    private String firstname;
    private String lastname;
    private String username;
    private String password;
    private Integer role;

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }
}
