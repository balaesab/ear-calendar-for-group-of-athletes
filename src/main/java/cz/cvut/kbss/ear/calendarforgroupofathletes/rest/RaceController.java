package cz.cvut.kbss.ear.calendarforgroupofathletes.rest;

import cz.cvut.kbss.ear.calendarforgroupofathletes.dto.RaceGetDto;
import cz.cvut.kbss.ear.calendarforgroupofathletes.dto.RaceOverviewGetDto;
import cz.cvut.kbss.ear.calendarforgroupofathletes.exception.NotFoundException;
import cz.cvut.kbss.ear.calendarforgroupofathletes.model.Race;
import cz.cvut.kbss.ear.calendarforgroupofathletes.model.User;
import cz.cvut.kbss.ear.calendarforgroupofathletes.rest.utils.RestUtils;
import cz.cvut.kbss.ear.calendarforgroupofathletes.service.RaceService;
import cz.cvut.kbss.ear.calendarforgroupofathletes.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/rest/races")
@CrossOrigin
public class RaceController {

    private static final Logger LOG = LoggerFactory.getLogger(RaceController.class);
    private final RaceService raceService;
    private final UserService userService;

    public RaceController(RaceService raceService, UserService userService) {
        this.raceService = raceService;
        this.userService = userService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public RaceOverviewGetDto getRaces() {
        return raceService.convertIntoRaceOverviewDto(raceService.findAll(), userService.getCurrentUser());
    }

    @GetMapping(value = "/racesOfCloseFriends", produces = MediaType.APPLICATION_JSON_VALUE)
    public HashMap<User, List<Race>> getRacesOfCloseFriends() {
        return userService.findRacesOfCloseFriends();
    }

    @DeleteMapping(value = "/{raceId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeRace(@PathVariable Integer raceId) {
        final Race toRemove = raceService.find(raceId);
        if (toRemove == null) {
            return;
        }
        raceService.remove(toRemove);
        LOG.debug("Cancelled race {}.", toRemove);
    }

    @GetMapping(value = "/{raceId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RaceGetDto getById(@PathVariable Integer raceId) {
        final Race race = raceService.find(raceId);
        if (race == null) {
            throw NotFoundException.create("Race", raceId);
        }
        return raceService.convertIntoRaceDto(race);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createRace(@RequestBody RaceGetDto raceDto) {
        Race race = raceService.convertIntoRace(raceDto);
        raceService.save(race);
        LOG.debug("Created race {}.", raceDto.getName());
        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", 8); //race.getId()
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }
}
