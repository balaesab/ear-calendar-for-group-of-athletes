package cz.cvut.kbss.ear.calendarforgroupofathletes.rest;

import cz.cvut.kbss.ear.calendarforgroupofathletes.utils.BeanPrinter;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/rest/test/beans")
@CrossOrigin
public class TestController {

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<String> getAndPrintBeans() {
        return BeanPrinter.printAndGetBeansNames();
    }
}
