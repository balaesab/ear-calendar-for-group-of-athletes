package cz.cvut.kbss.ear.calendarforgroupofathletes.security.model;

//base from e-shop
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.security.Principal;
import java.util.Collection;

public class AuthenticationToken extends AbstractAuthenticationToken implements Principal {

    private EarUserPrincipal earUserPrincipal;

    public AuthenticationToken(Collection<? extends GrantedAuthority> authorities, EarUserPrincipal earUserPrincipal) {
        super(authorities);
        this.earUserPrincipal = earUserPrincipal;
        super.setAuthenticated(true);
        super.setDetails(earUserPrincipal);
    }

    @Override
    public String getCredentials() {
        return earUserPrincipal.getPassword();
    }

    @Override
    public EarUserPrincipal getPrincipal() {
        return earUserPrincipal;
    }
}
