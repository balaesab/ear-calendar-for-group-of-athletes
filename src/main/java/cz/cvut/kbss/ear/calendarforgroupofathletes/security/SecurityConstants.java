package cz.cvut.kbss.ear.calendarforgroupofathletes.security;

//base from e-shop

public class SecurityConstants {
    public static final String REMEMBER_ME_COOKIE_NAME = "REMEMBER-ME";
    public static final String JSESSIONID_COOKIE_NAME = "JSESSIONID";

    public static final String USERNAME_PARAM = "username";
    public static final String PASSWORD_PARAM = "password";

    public static final String SECURITY_CHECK_URI = "/login";
    public static final String LOGOUT_URI = "/logout";
    public static final String REMEMBER_ME_COOKIE_GENERATION_KEY = "this is why this is so unique and meaningful at the same time";
}
