package cz.cvut.kbss.ear.calendarforgroupofathletes.dto;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

public class CompetitionGetDto {

    private String name;
    private List<Integer> racesIds;
    private List<String> racesNames;
    private List<LocalDate> racesDates;
    private List<LocalTime> racesTimes;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Integer> getRacesIds() {
        return racesIds;
    }

    public void setRacesIds(List<Integer> racesIds) {
        this.racesIds = racesIds;
    }

    public List<String> getRacesNames() {
        return racesNames;
    }

    public void setRacesNames(List<String> racesNames) {
        this.racesNames = racesNames;
    }

    public List<LocalDate> getRacesDates() {
        return racesDates;
    }

    public void setRacesDates(List<LocalDate> racesDates) {
        this.racesDates = racesDates;
    }

    public List<LocalTime> getRacesTimes() {
        return racesTimes;
    }

    public void setRacesTimes(List<LocalTime> racesTimes) {
        this.racesTimes = racesTimes;
    }
}
