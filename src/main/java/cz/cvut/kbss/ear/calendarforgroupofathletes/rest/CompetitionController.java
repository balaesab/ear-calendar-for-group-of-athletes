package cz.cvut.kbss.ear.calendarforgroupofathletes.rest;

import cz.cvut.kbss.ear.calendarforgroupofathletes.dto.CompetitionGetDto;
import cz.cvut.kbss.ear.calendarforgroupofathletes.dto.CompetitionPostDto;
import cz.cvut.kbss.ear.calendarforgroupofathletes.exception.NotFoundException;
import cz.cvut.kbss.ear.calendarforgroupofathletes.model.Competition;
import cz.cvut.kbss.ear.calendarforgroupofathletes.model.Race;
import cz.cvut.kbss.ear.calendarforgroupofathletes.rest.utils.RestUtils;
import cz.cvut.kbss.ear.calendarforgroupofathletes.service.CompetitionService;
import cz.cvut.kbss.ear.calendarforgroupofathletes.service.RaceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest/competitions")
@CrossOrigin
public class CompetitionController {

    private static final Logger LOG = LoggerFactory.getLogger(CompetitionController.class);

    private final CompetitionService competitionService;
    private final RaceService raceService;

    @Autowired
    public CompetitionController(CompetitionService service, RaceService raceService) {
        this.competitionService = service;
        this.raceService = raceService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Competition> getCompetitions() {
        return competitionService.findAll();
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public CompetitionGetDto getById(@PathVariable Integer id) {
        final Competition competition = competitionService.find(id);
        if (competition == null) {
            throw NotFoundException.create("Competition", id);
        }
        return competitionService.convertIntoCompetitionDto(competition);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createCompetition(@RequestBody CompetitionPostDto competitionPostDto) {
        Competition competition = competitionService.convertIntoCompetition(competitionPostDto);
        competitionService.save(competition);
        LOG.debug("Created competition {}.", competition);
        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", competition.getId());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeCompetition(@PathVariable Integer id) {
        final Competition toRemove = competitionService.find(id);
        if (toRemove == null) {
            return;
        }
        competitionService.remove(toRemove);
        LOG.debug("Removed competition {}.", toRemove);
    }

    //@PreAuthorize("hasRole('')")
    @PostMapping(value = "/{competitionId}/{raceId}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void addRaceToCompetition(@PathVariable Integer competitionId, @PathVariable Integer raceId) {
        final Competition competition = competitionService.find(competitionId);
        if (competition == null) {
            throw NotFoundException.create("Competition", raceId);
        }
        final Race race = raceService.find(raceId);
        if (race == null) {
            throw NotFoundException.create("Race", raceId);
        }
        competitionService.addRace(competition, race);
        LOG.debug("Race {} added into competition {}.", race, competition);
    }

    //@PreAuthorize("hasRole('')")
    @DeleteMapping(value = "/{competitionId}/races/{raceToRemoveId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeRaceFromCompetition(@PathVariable Integer competitionId, @PathVariable Integer raceToRemoveId) {
        final Competition competition = competitionService.find(competitionId);
        if (competition == null) {
            throw NotFoundException.create("Competition", competitionId);
        }
        final Race raceToRemove = raceService.find(raceToRemoveId);
        if (raceToRemove == null) {
            throw NotFoundException.create("Race", raceToRemoveId);
        }
        competitionService.removeRace(competition, raceToRemove);
        LOG.debug("Race {} removed from competition {}.", raceToRemove, competition);
    }
}
