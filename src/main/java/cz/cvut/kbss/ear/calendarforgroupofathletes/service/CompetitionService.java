package cz.cvut.kbss.ear.calendarforgroupofathletes.service;

import cz.cvut.kbss.ear.calendarforgroupofathletes.dao.BaseDao;
import cz.cvut.kbss.ear.calendarforgroupofathletes.dao.CompetitionDao;
import cz.cvut.kbss.ear.calendarforgroupofathletes.dao.RaceDao;
import cz.cvut.kbss.ear.calendarforgroupofathletes.dto.CompetitionGetDto;
import cz.cvut.kbss.ear.calendarforgroupofathletes.dto.CompetitionPostDto;
import cz.cvut.kbss.ear.calendarforgroupofathletes.model.Competition;
import cz.cvut.kbss.ear.calendarforgroupofathletes.model.Race;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@Transactional
public class CompetitionService extends BaseService<Competition> {

    @Autowired
    protected final RaceDao raceDao;

    @Autowired
    public CompetitionService(CompetitionDao dao, RaceDao raceDao) {
        super(dao);
        this.raceDao = raceDao;
    }

    public void addRace(Competition competition, Race raceToAdd) {
        Objects.requireNonNull(competition);
        Objects.requireNonNull(raceToAdd);
        competition.addRace(raceToAdd);
        dao.update(competition);
    }

    public void removeRace(Competition competition, Race raceToRemove) {
        Objects.requireNonNull(competition);
        Objects.requireNonNull(raceToRemove);
        competition.removeRace(raceToRemove);
        dao.update(competition);
    }

    public CompetitionGetDto convertIntoCompetitionDto(Competition competition) {
        CompetitionGetDto competitionGetDto = new CompetitionGetDto();

        competitionGetDto.setName(competition.getName());

        List<Race> races = competition.getRaces();

        List<Integer> racesIds = mapToCollectList(races.stream(), Race::getId);
        List<String> racesNames = mapToCollectList(races.stream(), Race::getName);
        List<LocalDate> racesDates = mapToCollectList(races.stream(), Race::getDate);
        List<LocalTime> racesTimes = mapToCollectList(races.stream(), Race::getTime);

        competitionGetDto.setRacesIds(racesIds);
        competitionGetDto.setRacesNames(racesNames);
        competitionGetDto.setRacesDates(racesDates);
        competitionGetDto.setRacesTimes(racesTimes);

        return competitionGetDto;
    }

    public Competition convertIntoCompetition(CompetitionPostDto competitionPostDto) {
        Competition competition = new Competition();

        competition.setName(competitionPostDto.getName());
        competition.setRaces(mapToCollectList(competitionPostDto.getRacesIds().stream(), raceDao::find));

        return competition;
    }
}
