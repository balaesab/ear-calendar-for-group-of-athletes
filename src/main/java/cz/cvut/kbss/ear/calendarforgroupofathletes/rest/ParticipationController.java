package cz.cvut.kbss.ear.calendarforgroupofathletes.rest;

import cz.cvut.kbss.ear.calendarforgroupofathletes.exception.NotFoundException;
import cz.cvut.kbss.ear.calendarforgroupofathletes.exception.ValidationException;
import cz.cvut.kbss.ear.calendarforgroupofathletes.model.Participation;
import cz.cvut.kbss.ear.calendarforgroupofathletes.model.User;
import cz.cvut.kbss.ear.calendarforgroupofathletes.rest.utils.RestUtils;
import cz.cvut.kbss.ear.calendarforgroupofathletes.service.ParticipationService;
import cz.cvut.kbss.ear.calendarforgroupofathletes.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest/participations")
@CrossOrigin
public class ParticipationController {
    private final ParticipationService participationService;
    private final UserService userService;
    private static final Logger LOG = LoggerFactory.getLogger(ParticipationController.class);

    public ParticipationController(ParticipationService participationService, UserService userService) {
        this.participationService = participationService;
        this.userService = userService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Participation> getParticipations() {
        return participationService.findAll();
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createParticipation(@RequestBody Participation participation) {
        participationService.save(participation);
        LOG.debug("Created participation {}.", participation);
        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", participation.getId());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Participation getById(@PathVariable Integer id) {
        final Participation participation = participationService.find(id);
        if (participation == null) {
            throw NotFoundException.create("Participation", id);
        }
        return participation;
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateParticipation(@PathVariable Integer id, @RequestBody Participation participation) {
        final Participation original = getById(id);
        if (!original.getId().equals(participation.getId())) {
            throw new ValidationException("Participation identifier in the data does not match the one in the request URL.");
        }
        participationService.update(participation);
        LOG.debug("Updated participation {}.", participation);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeParticipation(@PathVariable Integer id) {
        final Participation toRemove = participationService.find(id);
        if (toRemove == null) {
            return;
        }
        participationService.remove(toRemove);
        LOG.debug("Removed participation {}.", toRemove);
    }

    /*@PostMapping(value = "/{id}/racers", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void addParticipationToUser(@PathVariable Integer id, @RequestBody User user) {
        final Participation participation = getById(id);
        participationService.addParticipation(user, participation.getRace());
        LOG.debug("User {} added into race {}.", user, participation.getRace());
    }

    @DeleteMapping(value = "/{participationId}/racers/{racerId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeParticipationFromUser(@PathVariable Integer participationId,
                                          @PathVariable Integer racerId) {
        final Participation participation = getById(participationId);
        final User toRemove = userService.find(racerId);
        if (toRemove == null) {
            throw NotFoundException.create("Participation", participation);
        }
        participationService.removeParticipation(toRemove, participation.getRace());
        LOG.debug("Racer {} removed from participation {}.", toRemove, participation);
    }*/
}
