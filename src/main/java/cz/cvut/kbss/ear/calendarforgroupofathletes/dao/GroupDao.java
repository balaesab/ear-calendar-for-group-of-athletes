package cz.cvut.kbss.ear.calendarforgroupofathletes.dao;

import cz.cvut.kbss.ear.calendarforgroupofathletes.model.Group;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class GroupDao extends BaseDao<Group> {
    protected GroupDao() {
        super(Group.class);
    }

    public List<Group> findByName(String name) {
        return em.createNamedQuery("Group.findByName", Group.class).setParameter("name", name)
                .getResultList();
    }
}
