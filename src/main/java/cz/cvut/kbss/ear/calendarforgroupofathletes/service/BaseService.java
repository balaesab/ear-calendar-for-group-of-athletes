package cz.cvut.kbss.ear.calendarforgroupofathletes.service;

import cz.cvut.kbss.ear.calendarforgroupofathletes.dao.BaseDao;
import jdk.swing.interop.SwingInterOpUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Transactional
public class BaseService<T> {

    protected final BaseDao<T> dao;

    @Autowired
    public BaseService(BaseDao<T> dao) {
        this.dao = dao;
    }

    @Transactional(readOnly = true)
    public T find(Integer id) {
        return dao.find(id);
    }

    @Transactional(readOnly = true)
    public List<T> findAll() {
        return dao.findAll();
    }

    public void save(T abstractEntity) {
        dao.persist(abstractEntity);
    }

    public void update(T abstractEntity) {
        dao.update(abstractEntity);
    }

    public void remove(T abstractEntity) {
        dao.remove(abstractEntity);
    }

    protected <U, R> List<R> mapToCollectList(Stream<U> stream, Function<? super U, ? extends R> mapper) {
        return stream.map(mapper).collect(Collectors.toCollection(ArrayList::new));
    }
}
