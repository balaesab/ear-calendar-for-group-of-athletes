package cz.cvut.kbss.ear.calendarforgroupofathletes.dto;

import java.util.List;

public class GroupGetDto {

    private String name;
    private Integer creatorId;
    private String creatorUsername;
    private List<Integer> membersIds;
    private List<String> membersUsernames;
    private List<String> membersFirstNames;
    private List<String> membersLastNames;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Integer creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreatorUsername() {
        return creatorUsername;
    }

    public void setCreatorUsername(String creatorUsername) {
        this.creatorUsername = creatorUsername;
    }

    public List<Integer> getMembersIds() {
        return membersIds;
    }

    public void setMembersIds(List<Integer> membersIds) {
        this.membersIds = membersIds;
    }

    public List<String> getMembersUsernames() {
        return membersUsernames;
    }

    public void setMembersUsernames(List<String> membersUsernames) {
        this.membersUsernames = membersUsernames;
    }

    public List<String> getMembersFirstNames() {
        return membersFirstNames;
    }

    public void setMembersFirstNames(List<String> membersFirstNames) {
        this.membersFirstNames = membersFirstNames;
    }

    public List<String> getMembersLastNames() {
        return membersLastNames;
    }

    public void setMembersLastNames(List<String> membersLastNames) {
        this.membersLastNames = membersLastNames;
    }
}
