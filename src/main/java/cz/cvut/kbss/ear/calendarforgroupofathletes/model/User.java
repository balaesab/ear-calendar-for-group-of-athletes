package cz.cvut.kbss.ear.calendarforgroupofathletes.model;

import com.fasterxml.jackson.annotation.*;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "app_user")
@NamedQueries({
        @NamedQuery(name = "User.findByUsername", query = "SELECT u FROM User u WHERE u.username = :username"),
        @NamedQuery(name = "User.findByCharSequenceContainedInName", query =
                "SELECT u FROM User u WHERE LOWER(CONCAT(u.firstName, ' ', u.lastName)) LIKE LOWER(CONCAT('%', :sequence, '%'))"),
        @NamedQuery(name = "User.findByRole", query =
                "SELECT u FROM User u WHERE u.role = :role")
})
public class User extends AbstractEntity {
    @Basic(optional = false)
    @Column(nullable = false)
    protected String firstName;

    @Basic(optional = false)
    @Column(nullable = false)
    protected String lastName;

    @Basic(optional = false)
    @Column(nullable = false, unique = true)
    protected String username;

    @JsonIgnore
    @Basic(optional = false)
    @Column(nullable = false)
    protected String password;

    @Enumerated(value=EnumType.ORDINAL)
    @Basic(optional = false)
    @Column(nullable = false)
    private Role role;

    @JsonManagedReference
    @OneToMany(mappedBy = "participant")
    private List<Participation> participations;

    @JsonBackReference
    @ManyToMany(mappedBy = "members")
    @OrderBy("name")
    private List<Group> groups;

    @JsonIgnore
    @OneToMany
    @JoinTable(name = "close_friend",
            joinColumns = @JoinColumn(name = "ownerId"),
            inverseJoinColumns = @JoinColumn(name = "friendId"))
    @OrderBy("lastName")
    private List<User> closeFriends;

    public User() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void encodePassword(PasswordEncoder encoder) {
        this.password = encoder.encode(password);
    }

    public void erasePassword() {
        this.password = null;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public List<Participation> getParticipations() {
        return participations;
    }

    public void setParticipations(List<Participation> participations) {
        this.participations = participations;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    public List<User> getCloseFriends() {
        return closeFriends;
    }

    public void setCloseFriends(List<User> closeFriends) {
        this.closeFriends = closeFriends;
    }

    public void addParticipation(Participation participation) {
        Objects.requireNonNull(participation);
        if (participations == null) {
            participations = new ArrayList<>();
        }
        participations.add(participation);
    }

    public void removeParticipation(Participation participation) {
        Objects.requireNonNull(participation);
        if (participations == null) {
            return;
        }
        participations.removeIf(r -> Objects.equals(r.getId(), participation.getId()));
    }

    private void addGroup(Group group) {
        Objects.requireNonNull(group);
        if (groups == null) {
            this.groups = new ArrayList<>();
        }
        groups.add(group);
    }

    private void removeGroup(Group group) {
        Objects.requireNonNull(group);
        if (groups == null) {
            return;
        }
        groups.removeIf(g -> Objects.equals(g.getId(), group.getId()));
    }

    public void addCloseFriend(User user) {
        Objects.requireNonNull(user);
        if (closeFriends == null) {
            this.closeFriends = new ArrayList<>();
        }
        closeFriends.add(user);
    }

    public void removeCloseFriend(User user) {
        Objects.requireNonNull(user);
        if (closeFriends == null) {
            return;
        }
        groups.removeIf(u -> Objects.equals(u.getId(), user.getId()));
    }
}
