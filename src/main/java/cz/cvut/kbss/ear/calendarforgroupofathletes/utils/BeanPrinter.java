package cz.cvut.kbss.ear.calendarforgroupofathletes.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class BeanPrinter {

    static ApplicationContext applicationContext;

    @Autowired
    public BeanPrinter(ApplicationContext applicationContext) {
        BeanPrinter.applicationContext = applicationContext;
    }

    public static void printBeansNames() {
        System.out.println(getBeansNames());
    }

    public static List<String> getBeansNames() {
        return Arrays.asList(applicationContext.getBeanDefinitionNames());
    }

    public static List<String> printAndGetBeansNames() {
        List<String> beansNamesInString = getBeansNames();
        System.out.println(beansNamesInString);
        return beansNamesInString;
    }
}
