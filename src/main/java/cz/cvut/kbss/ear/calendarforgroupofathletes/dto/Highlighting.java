package cz.cvut.kbss.ear.calendarforgroupofathletes.dto;

public enum Highlighting {
    COMMON, FRIENDLY, SOLO, GENERAL;
}
