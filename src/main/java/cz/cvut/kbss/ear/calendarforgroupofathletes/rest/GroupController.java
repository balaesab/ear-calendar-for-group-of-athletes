package cz.cvut.kbss.ear.calendarforgroupofathletes.rest;

import cz.cvut.kbss.ear.calendarforgroupofathletes.dto.GroupGetDto;
import cz.cvut.kbss.ear.calendarforgroupofathletes.dto.GroupPostDto;
import cz.cvut.kbss.ear.calendarforgroupofathletes.exception.NotFoundException;
import cz.cvut.kbss.ear.calendarforgroupofathletes.model.Group;
import cz.cvut.kbss.ear.calendarforgroupofathletes.model.User;
import cz.cvut.kbss.ear.calendarforgroupofathletes.rest.utils.RestUtils;
import cz.cvut.kbss.ear.calendarforgroupofathletes.service.GroupService;
import cz.cvut.kbss.ear.calendarforgroupofathletes.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest/groups")
@CrossOrigin
public class GroupController {
    private final GroupService groupService;
    private final UserService userService;
    private static final Logger LOG = LoggerFactory.getLogger(GroupController.class);

    @Autowired
    public GroupController(GroupService groupService, UserService userService) {
        this.groupService = groupService;
        this.userService = userService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Group> getGroups() { return groupService.findAll(); }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('ROLE_COACH')")
    public ResponseEntity<Void> createGroup(@RequestBody GroupPostDto groupPostDto) {
        Group group = groupService.convertIntoGroup(groupPostDto);
        groupService.save(group);
        LOG.debug("Created group {}.", group);
        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", group.getId());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{groupId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public GroupGetDto getById(@PathVariable Integer groupId) {
        final Group group = groupService.find(groupId);
        if (group == null) {
            throw NotFoundException.create("Group", groupId);
        }
        return groupService.convertIntoGroupDto(group);
    }

    @DeleteMapping(value = "/{groupId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeGroup(@PathVariable Integer groupId) {
        final Group toRemove = groupService.find(groupId);
        if (toRemove == null) {
            return;
        }
        groupService.remove(toRemove);
        LOG.debug("Removed group {}.", toRemove);
    }

    @PostMapping(value = "/{groupId}/{memberId}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("principal.username==groupDao.find(groupId).creator.username")
    public void addMemberToGroup(@PathVariable Integer groupId, @PathVariable Integer memberId) {
        final Group group = groupService.find(groupId);
        if (group == null) {
            throw NotFoundException.create("Group", groupId);
        }
        final User member = userService.find(memberId);
        if (member == null) {
            throw NotFoundException.create("User", memberId);
        }
        groupService.addRacer(group, member);
        LOG.debug("Member {} added into group {}.", group, member);
    }

    @DeleteMapping(value = "/{groupId}/members/{memberId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("principal.username==groupDao.find(memberId).creator.username")
    public void removeMemberFromGroup(@PathVariable Integer groupId,
                                          @PathVariable Integer memberId) {
        final Group group = groupService.find(groupId);
        if (group == null) {
            throw NotFoundException.create("Group", groupId);
        }
        final User toRemove = userService.find(memberId);
        if (toRemove == null) {
            throw NotFoundException.create("Member", memberId);
        }
        groupService.removeRacer(group, toRemove);
        LOG.debug("Member {} removed from group {}.", toRemove, group);
    }

}
