package cz.cvut.kbss.ear.calendarforgroupofathletes.dao;

import cz.cvut.kbss.ear.calendarforgroupofathletes.model.Competition;
import cz.cvut.kbss.ear.calendarforgroupofathletes.model.Race;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;

@Repository
public class CompetitionDao extends BaseDao<Competition> {
    protected CompetitionDao() {
        super(Competition.class);
    }

    public Competition findByName(String name) {
        try {
            return em.createNamedQuery("Competition.findByName", Competition.class).setParameter("name", name)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}
