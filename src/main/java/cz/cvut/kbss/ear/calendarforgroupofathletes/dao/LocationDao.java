package cz.cvut.kbss.ear.calendarforgroupofathletes.dao;

import cz.cvut.kbss.ear.calendarforgroupofathletes.model.Location;
import org.springframework.stereotype.Repository;

@Repository
public class LocationDao extends BaseDao<Location> {
    protected LocationDao() {
        super(Location.class);
    }
}
