package cz.cvut.kbss.ear.calendarforgroupofathletes.service;

import cz.cvut.kbss.ear.calendarforgroupofathletes.dao.UserDao;
import cz.cvut.kbss.ear.calendarforgroupofathletes.dto.UserGetDto;
import cz.cvut.kbss.ear.calendarforgroupofathletes.dto.UserPostDto;
import cz.cvut.kbss.ear.calendarforgroupofathletes.model.*;
import cz.cvut.kbss.ear.calendarforgroupofathletes.model.Participation;
import cz.cvut.kbss.ear.calendarforgroupofathletes.model.Race;
import cz.cvut.kbss.ear.calendarforgroupofathletes.model.User;
import cz.cvut.kbss.ear.calendarforgroupofathletes.security.SecurityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.*;

@Service
@Transactional
public class UserService extends BaseService<User> {

    private final PasswordEncoder passwordEncoder;

    public UserService(UserDao dao, PasswordEncoder passwordEncoder) {
        super(dao);
        this.passwordEncoder = passwordEncoder;
    }

    public void save(User user) {
        Objects.requireNonNull(user);
        user.encodePassword(passwordEncoder);

        Objects.requireNonNull(user.getFirstName());
        Objects.requireNonNull(user.getLastName());
        Objects.requireNonNull(user.getLastName());
        Objects.requireNonNull(user.getUsername());
        Objects.requireNonNull(user.getPassword());
        Objects.requireNonNull(user.getRole());
        Objects.requireNonNull(user.getGroups());
        Objects.requireNonNull(user.getRole());
        Objects.requireNonNull(user.getCloseFriends());
        if (user.getParticipations() == null) {
            user.setParticipations(new ArrayList<>());
        }

        dao.persist(user);
    }

    @Transactional(readOnly = true)
    public boolean exists(String username) {
        return ((UserDao) dao).findByUsername(username) != null;
    }

    public User getCurrentUser() {
        return SecurityUtils.getCurrentUser();
    }

    public HashMap<User, List<Race>> findRacesOfCloseFriends() {
        HashMap<User, List<Race>> racesOfFriends = new HashMap<>();

        List<User> closeFriends = getCurrentUser().getCloseFriends();
        for (User friend : closeFriends) {
            List<Race> races = new ArrayList<>();
            for (Participation participation : friend.getParticipations()) {
                races.add(participation.getRace());
            }
            racesOfFriends.put(friend, races);
        }
        return racesOfFriends;
    }

    public HashMap<Race, List<User>> findRacesToCloseFriends() {

        HashMap<Race, List<User>> racesToCloseFriends = new HashMap<>();

        User currentUser = getCurrentUser();
        List<User> closeFriends = currentUser == null ? new ArrayList<>() : currentUser.getCloseFriends();
        for (User friend : closeFriends) {

            for (Participation participation : friend.getParticipations()) {
                Race raceWithFriend = participation.getRace();

                List<User> closeFriendsParticipants = racesToCloseFriends.getOrDefault(raceWithFriend, new ArrayList<>());
                closeFriendsParticipants.add(friend);
                racesToCloseFriends.putIfAbsent(raceWithFriend, closeFriendsParticipants);
            }

        }

        return racesToCloseFriends;
    }

    @Transactional
    public void addCloseFriend(User friendToAdd) {
        Objects.requireNonNull(friendToAdd);
        User currentUser = getCurrentUser();
        currentUser.addCloseFriend(friendToAdd);
        dao.update(currentUser);
    }

    @Transactional
    public void removeCloseFriend(User friendToRemove) {
        Objects.requireNonNull(friendToRemove);
        User currentUser = getCurrentUser();
        currentUser.addCloseFriend(friendToRemove);
        dao.update(currentUser);
    }

    public UserGetDto convertIntoUserDto(User user) {
        UserGetDto userGetDto = new UserGetDto();

        userGetDto.setUsername(user.getUsername());
        userGetDto.setFirstName(user.getFirstName());
        userGetDto.setLastName(user.getLastName());
        userGetDto.setRoleOrdinal(user.getRole().ordinal());

        List<Participation> participations = user.getParticipations();
        List<Race> attendedRaces = mapToCollectList(participations.stream(), Participation::getRace);
        List<Competition> attendedRacesCompetitions = mapToCollectList(attendedRaces.stream(), Race::getCompetition);

        List<Integer> attendedRacesIds = mapToCollectList(attendedRaces.stream(), Race::getId);
        List<String> attendedRacesNames = mapToCollectList(attendedRaces.stream(), Race::getName);
        List<LocalDate> attendedRacesDates = mapToCollectList(attendedRaces.stream(), Race::getDate);
        List<Integer> attendedRacesCompetitionsIds = mapToCollectList(attendedRacesCompetitions.stream(), Competition::getId);
        List<String> attendedRacesCompetitionsNames = mapToCollectList(attendedRacesCompetitions.stream(), Competition::getName);
        List<Double> attendedRacesTimes = mapToCollectList(participations.stream(), Participation::getResultTimeInSeconds);

        userGetDto.setAttendedRacesIds(attendedRacesIds);
        userGetDto.setAttendedRacesNames(attendedRacesNames);
        userGetDto.setAttendedRacesDates(attendedRacesDates);
        userGetDto.setAttendedRacesCompetitionsIds(attendedRacesCompetitionsIds);
        userGetDto.setAttendedRacesCompetitionsNames(attendedRacesCompetitionsNames);
        userGetDto.setAttendedRacesTimes(attendedRacesTimes);

        List<Group> groups = user.getGroups();

        List<Integer> groupsIds = mapToCollectList(groups.stream(), Group::getId);
        List<String> groupsNames = mapToCollectList(groups.stream(), Group::getName);

        userGetDto.setGroupsIds(groupsIds);
        userGetDto.setGroupsNames(groupsNames);

        List<User> closeFriends = user.getCloseFriends();

        List<Integer> closeFriendsIds = mapToCollectList(closeFriends.stream(), User::getId);
        List<String> closeFriendsUsernames = mapToCollectList(closeFriends.stream(), User::getUsername);

        userGetDto.setCloseFriendsIds(closeFriendsIds);
        userGetDto.setCloseFriendsUsernames(closeFriendsUsernames);

        return userGetDto;
    }

    public User convertIntoUser(UserPostDto userPostDto) {
        User user = new User();

        user.setFirstName(userPostDto.getFirstname());
        user.setLastName(userPostDto.getLastname());
        user.setUsername(userPostDto.getUsername());
        user.setPassword(userPostDto.getPassword());
        user.setRole(Role.values()[userPostDto.getRole()]);

        return user;
    }
}
