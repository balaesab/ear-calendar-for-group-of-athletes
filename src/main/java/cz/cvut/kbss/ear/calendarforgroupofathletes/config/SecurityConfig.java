package cz.cvut.kbss.ear.calendarforgroupofathletes.config;

//base from e-shop

import cz.cvut.kbss.ear.calendarforgroupofathletes.security.SecurityConstants;
import cz.cvut.kbss.ear.calendarforgroupofathletes.service.UserService;
import cz.cvut.kbss.ear.calendarforgroupofathletes.service.security.EarUserDetailsService;
import org.apache.tomcat.util.file.ConfigurationSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
// Allow methods to be secured using annotation*/
public class SecurityConfig /*extends WebSecurityConfigurerAdapter*/ {

    private static final String[] COOKIES_TO_DESTROY = {
            SecurityConstants.REMEMBER_ME_COOKIE_NAME,
            SecurityConstants.JSESSIONID_COOKIE_NAME
    };

    private final AuthenticationProvider authenticationProvider;
    private final AuthenticationSuccessHandler authenticationSuccessHandler;
    private final AuthenticationFailureHandler authenticationFailureHandler;
    private final LogoutSuccessHandler logoutSuccessHandler;
    private final CorsConfigurationSource corsConfigurationSource;

    @Autowired
    public SecurityConfig(AuthenticationFailureHandler authenticationFailureHandler,
                          AuthenticationSuccessHandler authenticationSuccessHandler,
                          LogoutSuccessHandler logoutSuccessHandler,
                          AuthenticationProvider authenticationProvider,
                          CorsConfigurationSource corsConfigurationSource) {
        this.authenticationProvider = authenticationProvider;
        this.authenticationSuccessHandler = authenticationSuccessHandler;
        this.authenticationFailureHandler = authenticationFailureHandler;
        this.logoutSuccessHandler = logoutSuccessHandler;
        this.corsConfigurationSource = corsConfigurationSource;
    }

//    @B/ean
//    public AuthenticationManager authenticationManager(
//            AuthenticationConfiguration configuration) throws Exception {
//        return configuration.getAuthenticationManager();
//    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http
                .cors().configurationSource(corsConfigurationSource)
                .and()
                .authorizeRequests()
                .anyRequest()
                .permitAll()
//                .authenticated()               //login permitted lower in this building sequence
                .and()
                .exceptionHandling().authenticationEntryPoint(new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED))
                .and()
                .headers().frameOptions().sameOrigin()
                .and()
                .authenticationProvider(authenticationProvider)            //adds an AuthenticationProvider to be used
                .csrf().disable()                           //disables CSRF protection
                .formLogin()                                //makes authentication form based
//              .loginPage("/login")                      //default login page generated if login page is not specified this way
                .permitAll()
                .successHandler(authenticationSuccessHandler)
                .failureHandler(authenticationFailureHandler)
                .loginProcessingUrl(SecurityConstants.SECURITY_CHECK_URI)       //by this we maybe say that we implement the processing (on this URL)
//              .defaultSuccessUrl("/homepage", true)      //set false if you don't want to be redirected every time
//              .failureUrl("/login?error=true")
                .usernameParameter(SecurityConstants.USERNAME_PARAM)
                .passwordParameter(SecurityConstants.PASSWORD_PARAM)        //The HTTP parameters to look for when performing authentication
                .and()
                .logout().invalidateHttpSession(true).deleteCookies(COOKIES_TO_DESTROY)
                .logoutUrl(SecurityConstants.LOGOUT_URI).logoutSuccessHandler(logoutSuccessHandler)
                .and()
                .rememberMe()
                .rememberMeParameter("REMEMBER-ME")
                .key(SecurityConstants.REMEMBER_ME_COOKIE_GENERATION_KEY).tokenValiditySeconds(90)
                .and()
                .sessionManagement().maximumSessions(1);
        return http.build();
    }
}
