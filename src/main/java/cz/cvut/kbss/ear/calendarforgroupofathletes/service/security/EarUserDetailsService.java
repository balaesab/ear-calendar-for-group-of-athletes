package cz.cvut.kbss.ear.calendarforgroupofathletes.service.security;

//base from e-shop
import cz.cvut.kbss.ear.calendarforgroupofathletes.dao.UserDao;
import cz.cvut.kbss.ear.calendarforgroupofathletes.model.User;
import cz.cvut.kbss.ear.calendarforgroupofathletes.security.model.EarUserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class EarUserDetailsService implements UserDetailsService {

    private final UserDao userDao;

    @Autowired
    public EarUserDetailsService(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public EarUserPrincipal loadUserByUsername(String username) throws UsernameNotFoundException {
        final User user = userDao.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("User with username " + username + " not found.");
        }
        return new EarUserPrincipal(user);
    }
}