package cz.cvut.kbss.ear.calendarforgroupofathletes.exception;

public class InvalidTimestampException extends EarException {

    public InvalidTimestampException(String message) {
        super(message);
    }

    public InvalidTimestampException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidTimestampException(Throwable cause) {
        super(cause);
    }
}
