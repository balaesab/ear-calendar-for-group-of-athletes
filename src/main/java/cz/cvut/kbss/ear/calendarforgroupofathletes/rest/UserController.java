package cz.cvut.kbss.ear.calendarforgroupofathletes.rest;

import cz.cvut.kbss.ear.calendarforgroupofathletes.dto.MyGroupsGetDto;
import cz.cvut.kbss.ear.calendarforgroupofathletes.dto.UserGetDto;
import cz.cvut.kbss.ear.calendarforgroupofathletes.dto.UserPostDto;
import cz.cvut.kbss.ear.calendarforgroupofathletes.exception.NotFoundException;
import cz.cvut.kbss.ear.calendarforgroupofathletes.model.User;
import cz.cvut.kbss.ear.calendarforgroupofathletes.rest.utils.RestUtils;
import cz.cvut.kbss.ear.calendarforgroupofathletes.service.GroupService;
import cz.cvut.kbss.ear.calendarforgroupofathletes.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest/users")
@CrossOrigin
public class UserController {

    private static final Logger LOG = LoggerFactory.getLogger(UserController.class);

    private final UserService userService;
    private final GroupService groupService;

    @Autowired
    public UserController(UserService userService, GroupService groupService){
        this.userService = userService;
        this.groupService = groupService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<User> getUsers() {
        return userService.findAll();
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public UserGetDto getById(@PathVariable Integer id) {
        final User user = userService.find(id);
        if (user == null) {
            throw NotFoundException.create("User", id);
        }
        return userService.convertIntoUserDto(user);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createUser(@RequestBody UserPostDto userPostDto) {
        User user = userService.convertIntoUser(userPostDto);
        userService.save(user);
        LOG.debug("Created user {}.", user);
        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", user.getId());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeUser(@PathVariable Integer id) {
        final User toRemove = userService.find(id);
        if (toRemove == null) {
            return;
        }
        userService.remove(toRemove);
        LOG.debug("Removed user {}.", toRemove);
    }

    @GetMapping(value = "/membership", produces = MediaType.APPLICATION_JSON_VALUE)
    public MyGroupsGetDto getCurrentUsersGroups() {
        final User user = userService.getCurrentUser();
        if (user == null) {
            throw NotFoundException.create("Current user", "current session");
        }
        return groupService.convertIntoMyGroupsGetDto(user.getGroups());
    }

    @GetMapping(value = "/{userId}/membership", produces = MediaType.APPLICATION_JSON_VALUE)
    public MyGroupsGetDto getCurrentUsersGroups(@PathVariable Integer userId) {
        final User user = userService.find(userId);
        if (user == null) {
            throw NotFoundException.create("User", userId);
        }
        return groupService.convertIntoMyGroupsGetDto(user.getGroups());
    }

    @GetMapping(value = "/closeFriends", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<User> getCloseFriends() {
        return userService.getCurrentUser().getCloseFriends();
    }

    @PostMapping(value = "/closeFriends/{newFriendId}")
    public void addCloseFriend(@PathVariable Integer newFriendId) {
        User newFriend = userService.find(newFriendId);
        userService.addCloseFriend(newFriend);
        LOG.debug("User {} added to list of close friends.", newFriend);
    }

    @DeleteMapping(value = "/closeFriends/{friendId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeCloseFriend(@PathVariable Integer friendId) {
        final User toRemove = userService.find(friendId);
        if (toRemove == null) {
            throw NotFoundException.create("Friend", friendId);
        }
        userService.removeCloseFriend(toRemove);
        LOG.debug("User {} removed from list of close friends.", toRemove);
    }
}
