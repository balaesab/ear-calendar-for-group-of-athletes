package cz.cvut.kbss.ear.calendarforgroupofathletes.dao;

import cz.cvut.kbss.ear.calendarforgroupofathletes.model.Role;
import cz.cvut.kbss.ear.calendarforgroupofathletes.model.User;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class UserDao extends BaseDao<User> {
    protected UserDao() {
        super(User.class);
    }

    public User findByUsername(String username) {
        try {
            return em.createNamedQuery("User.findByUsername", User.class).setParameter("username", username)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public List<User> findByCharSequenceContainedInName(String sequence) {
        return em.createNamedQuery("User.findByCharSequenceContainedInName", User.class).setParameter("sequence", sequence).getResultList();
    }

    public List<User> findByRole(Role role) {
        return em.createNamedQuery("User.findByRole", User.class).setParameter("role", role.toString()).getResultList();
    }
}
