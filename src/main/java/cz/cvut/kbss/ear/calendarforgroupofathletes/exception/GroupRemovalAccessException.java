package cz.cvut.kbss.ear.calendarforgroupofathletes.exception;

public class GroupRemovalAccessException extends EarException {
    public GroupRemovalAccessException(String message) {
        super(message);
    }
}
