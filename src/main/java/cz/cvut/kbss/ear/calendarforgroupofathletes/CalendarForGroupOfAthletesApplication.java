package cz.cvut.kbss.ear.calendarforgroupofathletes;

import cz.cvut.kbss.ear.calendarforgroupofathletes.config.AppConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class CalendarForGroupOfAthletesApplication {

    public static void main(String[] args) {
        SpringApplication.run(CalendarForGroupOfAthletesApplication.class, args);
    }

}
