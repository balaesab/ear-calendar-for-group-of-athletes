package cz.cvut.kbss.ear.calendarforgroupofathletes.dto;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

public class RaceGetDto {

    private String name;
    private LocalDate date;
    private LocalTime time;
    private int startingFee;
    private String note;

    private String locationName;
    private String locationLink;

    private List<String> participatingUsernames;
    private List<String> participatingFirstNames;
    private List<String> participatingLastNames;

    private Integer competitionId;
    private String competitionName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public int getStartingFee() {
        return startingFee;
    }

    public void setStartingFee(int startingFee) {
        this.startingFee = startingFee;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getLocationLink() {
        return locationLink;
    }

    public void setLocationLink(String locationLink) {
        this.locationLink = locationLink;
    }

    public List<String> getParticipatingUsernames() {
        return participatingUsernames;
    }

    public void setParticipatingUsernames(List<String> participatingUsernames) {
        this.participatingUsernames = participatingUsernames;
    }

    public List<String> getParticipatingFirstNames() {
        return participatingFirstNames;
    }

    public void setParticipatingFirstNames(List<String> participatingFirstNames) {
        this.participatingFirstNames = participatingFirstNames;
    }

    public List<String> getParticipatingLastNames() {
        return participatingLastNames;
    }

    public void setParticipatingLastNames(List<String> participatingLastNames) {
        this.participatingLastNames = participatingLastNames;
    }

    public Integer getCompetitionId() {
        return competitionId;
    }

    public void setCompetitionId(Integer competitionId) {
        this.competitionId = competitionId;
    }

    public String getCompetitionName() {
        return competitionName;
    }

    public void setCompetitionName(String competitionName) {
        this.competitionName = competitionName;
    }
}
