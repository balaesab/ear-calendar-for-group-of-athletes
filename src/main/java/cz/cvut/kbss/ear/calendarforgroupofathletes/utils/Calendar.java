package cz.cvut.kbss.ear.calendarforgroupofathletes.utils;

import java.time.LocalDate;
import java.time.LocalTime;

public class Calendar {
    public static LocalDate getCurrentDate() {
        return LocalDate.now();
    }

    public static LocalTime getCurrentTime() {
        return LocalTime.now();
    }
}
