package cz.cvut.kbss.ear.calendarforgroupofathletes.service;

import cz.cvut.kbss.ear.calendarforgroupofathletes.dao.ParticipationDao;
import cz.cvut.kbss.ear.calendarforgroupofathletes.dao.RaceDao;
import cz.cvut.kbss.ear.calendarforgroupofathletes.dao.UserDao;
import cz.cvut.kbss.ear.calendarforgroupofathletes.exception.InvalidTimestampException;
import cz.cvut.kbss.ear.calendarforgroupofathletes.model.*;
import cz.cvut.kbss.ear.calendarforgroupofathletes.utils.Calendar;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

@Service
@Transactional
public class ParticipationService extends BaseService<Participation> {

    private final RaceDao raceDao;
    private final UserDao userDao;

    public ParticipationService(ParticipationDao dao, RaceDao raceDao, UserDao userDao) {
        super(dao);
        this.raceDao = raceDao;
        this.userDao = userDao;
    }

    public void setRacer(Participation participation, Race race) {
        Objects.requireNonNull(participation);
        Objects.requireNonNull(race);
        participation.setRace(race);
        dao.update(participation);
    }

    public void setParticipant(Participation participation, User participant) {
        Objects.requireNonNull(participation);
        Objects.requireNonNull(participant);
        participation.setParticipant(participant);
        dao.update(participation);
    }

    public void addParticipation(User participant, Race race) {
        Objects.requireNonNull(participant);
        Objects.requireNonNull(race);

        if (race.getDate().compareTo(Calendar.getCurrentDate()) > 0) {
            Participation participation = new Participation(participant, race);

            participant.addParticipation(participation);
            race.addParticipation(participation);

            dao.persist(participation);
            userDao.update(participant);
            raceDao.update(race);

        } else {
            throw new InvalidTimestampException("You can't enroll in this race because race was already held.");
        }
    }

    public void removeParticipation(User participant, Race race) {
        Objects.requireNonNull(participant);
        Objects.requireNonNull(race);

        if (race.getDate().compareTo(Calendar.getCurrentDate()) > 0) {

            Participation participation = ((ParticipationDao) dao).findByRaceAndParticipantId(participant.getId(), race.getId());

            participant.removeParticipation(participation);
            race.removeParticipation(participation);

            dao.remove(participation);
            userDao.update(participant);
            raceDao.update(race);

        } else {
            throw new InvalidTimestampException("You can't withdraw from this race because race was already held.");
        }
    }
}
