package cz.cvut.kbss.ear.calendarforgroupofathletes.security;

//base from e-shop
import cz.cvut.kbss.ear.calendarforgroupofathletes.security.model.AuthenticationToken;
import cz.cvut.kbss.ear.calendarforgroupofathletes.security.model.EarUserPrincipal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class DefaultAuthenticationProvider implements AuthenticationProvider {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultAuthenticationProvider.class);

    private final UserDetailsService userDetailsService;

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public DefaultAuthenticationProvider(@Lazy UserDetailsService userDetailsService, PasswordEncoder passwordEncoder) {
        this.userDetailsService = userDetailsService;
        this.passwordEncoder = passwordEncoder;
    }

    /*Important security-related classes and notions used in the e-shop involve
        SecurityConfig and all the beans used in it
        SecurityUtils - Namely, the thread local-based SecurityContextHolder
        Aforementioned Spring security annotations
    Use SecurityUtils, UserDetails, and UserDetailsService classes in the method implementation.*/
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String inputCredentials = authentication.getCredentials().toString();
        String username = authentication.getName();
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        EarUserPrincipal authenticatedEarUserPrincipal = (EarUserPrincipal) userDetails;
        String password = authenticatedEarUserPrincipal.getPassword();
        if (!passwordEncoder.matches(inputCredentials, password)) {
            throw new BadCredentialsException("Credentials don't match.");
        }
        authenticatedEarUserPrincipal.eraseCredentials();
        return SecurityUtils.setCurrentUser(authenticatedEarUserPrincipal);
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return UsernamePasswordAuthenticationToken.class.isAssignableFrom(aClass) ||
                AuthenticationToken.class.isAssignableFrom(aClass);
    }
}
