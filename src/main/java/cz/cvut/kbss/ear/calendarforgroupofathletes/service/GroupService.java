package cz.cvut.kbss.ear.calendarforgroupofathletes.service;

import cz.cvut.kbss.ear.calendarforgroupofathletes.dao.GroupDao;
import cz.cvut.kbss.ear.calendarforgroupofathletes.dao.UserDao;
import cz.cvut.kbss.ear.calendarforgroupofathletes.dto.GroupGetDto;
import cz.cvut.kbss.ear.calendarforgroupofathletes.dto.GroupPostDto;
import cz.cvut.kbss.ear.calendarforgroupofathletes.dto.MyGroupsGetDto;
import cz.cvut.kbss.ear.calendarforgroupofathletes.exception.GroupRemovalAccessException;
import cz.cvut.kbss.ear.calendarforgroupofathletes.model.Group;
import cz.cvut.kbss.ear.calendarforgroupofathletes.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class GroupService extends BaseService<Group> {

    private final UserDao userDao;
    private final UserService userService;

    @Autowired
    public GroupService(GroupDao dao, UserDao userDao, UserService userService) {
        super(dao);
        this.userDao = userDao;
        this.userService = userService;
    }

    @Transactional
    public void addRacer(Group group, User memberToAdd) {
        Objects.requireNonNull(memberToAdd);
        Objects.requireNonNull(memberToAdd);
        group.addMember(memberToAdd);
        dao.update(group);
    }

    @Transactional
    public void removeRacer(Group group, User memberToRemove) {
        Objects.requireNonNull(memberToRemove);
        Objects.requireNonNull(memberToRemove);
        group.removeMember(memberToRemove);
        dao.update(group);
    }

    public void remove(Group group) {
        if (!group.getCreator().equals(userService.getCurrentUser())) {
            throw new GroupRemovalAccessException("You are not the creator of the group, so you can't remove it.");
        }
        dao.update(group);
    }


    public GroupGetDto convertIntoGroupDto(Group group) {
        GroupGetDto groupGetDto = new GroupGetDto();

        groupGetDto.setName(group.getName());
        groupGetDto.setCreatorId(group.getId());
        groupGetDto.setCreatorUsername(group.getCreator().getUsername());

        List<User> members = group.getMembers();

        List<Integer> membersIds = mapToCollectList(members.stream(), User::getId);
        List<String> membersUsernames = mapToCollectList(members.stream(), User::getUsername);
        List<String> membersFirstNames = mapToCollectList(members.stream(), User::getFirstName);
        List<String> membersLastNames = mapToCollectList(members.stream(), User::getLastName);

        groupGetDto.setMembersIds(membersIds);
        groupGetDto.setMembersUsernames(membersUsernames);
        groupGetDto.setMembersFirstNames(membersFirstNames);
        groupGetDto.setMembersLastNames(membersLastNames);

        return groupGetDto;
    }

    public Group convertIntoGroup(GroupPostDto groupPostDto) {
        Group group = new Group();

        group.setName(groupPostDto.getName());
        group.setCreator(userDao.find(groupPostDto.getCreatorId()));

        return group;
    }

    public MyGroupsGetDto convertIntoMyGroupsGetDto(List<Group> myGroups) {
        MyGroupsGetDto myGroupsGetDto = new MyGroupsGetDto();

        List<Integer> ids = mapToCollectList(myGroups.stream(), Group::getId);
        List<String> names = mapToCollectList(myGroups.stream(), Group::getName);
        List<Integer> creatorsId = mapToCollectList(myGroups.stream(), group -> group.getCreator().getId());
        List<String> creatorsUsernames = mapToCollectList(myGroups.stream(), group -> group.getCreator().getUsername());
        List<Integer> numbersOfMembers = mapToCollectList(myGroups.stream(), group -> group.getMembers().size());

        myGroupsGetDto.setIds(ids);
        myGroupsGetDto.setNames(names);
        myGroupsGetDto.setCreatorsIds(creatorsId);
        myGroupsGetDto.setCreatorsUsernames(creatorsUsernames);
        myGroupsGetDto.setNumbersOfMembers(numbersOfMembers);

        return myGroupsGetDto;
    }
}
