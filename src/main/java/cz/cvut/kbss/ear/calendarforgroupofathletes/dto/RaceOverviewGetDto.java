package cz.cvut.kbss.ear.calendarforgroupofathletes.dto;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

public class RaceOverviewGetDto {

    private List<Integer> racesIds;
    private List<String> racesNames;
    private List<LocalDate> racesDates;
    private List<LocalTime> racesTimes;
    private List<String> raceLocationsNames;
    //private List<List<String>> racesCloseFriendsParticipantsUsernames;

    private List<Integer> competitionsIds;
    private List<String> competitionsNames;

    private List<Highlighting> highlightingValues;

    public List<Integer> getRacesIds() {
        return racesIds;
    }

    public void setRacesIds(List<Integer> racesIds) {
        this.racesIds = racesIds;
    }

    public List<String> getRacesNames() {
        return racesNames;
    }

    public void setRacesNames(List<String> racesNames) {
        this.racesNames = racesNames;
    }

    public List<LocalDate> getRacesDates() {
        return racesDates;
    }

    public void setRacesDates(List<LocalDate> racesDates) {
        this.racesDates = racesDates;
    }

    public List<LocalTime> getRacesTimes() {
        return racesTimes;
    }

    public void setRacesTimes(List<LocalTime> racesTimes) {
        this.racesTimes = racesTimes;
    }

    public List<String> getRaceLocationsNames() {
        return raceLocationsNames;
    }

    public void setRaceLocationsNames(List<String> raceLocationsNames) {
        this.raceLocationsNames = raceLocationsNames;
    }

//    public List<List<String>> getRacesCloseFriendsParticipantsUsernames() {
//        return racesCloseFriendsParticipantsUsernames;
//    }
//
//    public void setRacesCloseFriendsParticipantsUsernames(List<List<String>> racesCloseFriendsParticipantsUsernames) {
//        this.racesCloseFriendsParticipantsUsernames = racesCloseFriendsParticipantsUsernames;
//    }

    public List<Integer> getCompetitionsIds() {
        return competitionsIds;
    }

    public void setCompetitionsIds(List<Integer> competitionsIds) {
        this.competitionsIds = competitionsIds;
    }

    public List<String> getCompetitionsNames() {
        return competitionsNames;
    }

    public void setCompetitionsNames(List<String> competitionsNames) {
        this.competitionsNames = competitionsNames;
    }

    public List<Highlighting> getHighlightingValues() {
        return highlightingValues;
    }

    public void setHighlightingValues(List<Highlighting> highlightingValues) {
        this.highlightingValues = highlightingValues;
    }
}
