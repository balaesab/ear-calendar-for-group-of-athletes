package cz.cvut.kbss.ear.calendarforgroupofathletes.model;

import javax.persistence.*;

@Entity
@Table(name = "location")
@Inheritance(strategy = InheritanceType.JOINED) //jeste zjistit
@DiscriminatorColumn(name = "type",
        discriminatorType = javax.persistence.DiscriminatorType.INTEGER)
public abstract class Location extends AbstractEntity {

    private String name;
    private String detailInfo;

    public abstract String[] getLinkParts();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetailInfo() {
        return detailInfo;
    }

    public void setDetailInfo(String detailInfo) {
        this.detailInfo = detailInfo;
    }
}
