package cz.cvut.kbss.ear.calendarforgroupofathletes.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name="race")
@NamedQueries({
        @NamedQuery(name = "Race.findSimilarByName", query = "SELECT r FROM Race r WHERE r.name LIKE :name"),
        @NamedQuery(name = "Race.findByDateAndTimeRange", query = "SELECT r FROM Race r WHERE r.date > :from AND r.date < :to")
})
public class Race extends AbstractEntity {
    private String name;

    //@Temporal(TemporalType.DATE)
    private LocalDate date;

    //@Temporal(TemporalType.TIME)
    private LocalTime time;

    @ManyToOne
    @JoinColumn(name = "locationId")
    private Location location;

    private int startingFee;
    private String note;

    private boolean isRemoved;

    //@OrderBy("participant.lastName")
    @OneToMany(mappedBy="race", cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<Participation> participations;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "competition")
    private Competition competition;

    public Race() {
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public int getStartingFee() {
        return startingFee;
    }

    public void setStartingFee(int startingFee) {
        this.startingFee = startingFee;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Competition getCompetition() {
        return competition;
    }

    public void setCompetition(Competition competition) {
        this.competition = competition;
    }

    public Integer getNumberOfRacers() { return participations.size(); }

    public Boolean isRemoved() {
        return isRemoved;
    }

    public void setRemoved(Boolean isRemoved) {
        this.isRemoved = isRemoved;
    }

    public void addParticipation(Participation participation) {
        Objects.requireNonNull(participation);
        if (participations == null) {
            this.participations = new ArrayList<>();
        }
        participations.add(participation);
    }

    public void removeParticipation(Participation participation) {
        Objects.requireNonNull(participation);
        if (participations == null) {
            return;
        }

        participations.removeIf(p -> Objects.equals(p.getId(), participation.getId()));
    }

    public List<Participation> getParticipations() {
        return participations;
    }

    public void setParticipations(List<Participation> participations) {
        this.participations = participations;
    }
}
