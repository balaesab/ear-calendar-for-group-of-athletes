package cz.cvut.kbss.ear.calendarforgroupofathletes.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "\"group\"")
@NamedQueries({
        @NamedQuery(name = "Group.findByName", query = "SELECT g FROM Group g WHERE g.name = :name")
})
public class Group extends AbstractEntity {

    @Basic(optional = false)
    @Column(nullable = false)
    private String name;

    @JsonManagedReference
    @ManyToOne
    @JoinColumn(name="creator", nullable = false)
    private User creator;

    @ManyToMany
    @JoinTable(name="group_member",
            joinColumns=
            @JoinColumn(name="group"),
            inverseJoinColumns=
            @JoinColumn(name="member"))
    private List<User> members;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public List<User> getMembers() {
        return members;
    }

    public void setMembers(List<User> members) {
        this.members = members;
    }

    public void addMember(User member) {
        Objects.requireNonNull(member);
        if (members == null) {
            this.members = new ArrayList<>();
        }
        members.add(member);
    }

    public void removeMember(User member) {
        Objects.requireNonNull(member);
        if (members == null) {
            return;
        }
        members.removeIf(m -> Objects.equals(m.getId(), member.getId()));
    }
}
