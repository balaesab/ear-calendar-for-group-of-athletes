package cz.cvut.kbss.ear.calendarforgroupofathletes.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name="competition")
@NamedQueries({
        @NamedQuery(name = "Competition.findByName", query = "SELECT c FROM Competition c WHERE c.name = :name")
})
public class Competition extends AbstractEntity {

    private String name;

    @JsonManagedReference
    @OneToMany(mappedBy = "competition")
    @OrderBy("date")
    private List<Race> races;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Race> getRaces() {
        return races;
    }

    public void setRaces(List<Race> races) {
        this.races = races;
    }

    public void addRace(Race race) {
        Objects.requireNonNull(race);
        if (races == null) {
            this.races = new ArrayList<>();
        }
        races.add(race);
    }

    public void removeRace(Race race) {
        Objects.requireNonNull(race);
        if (races == null) {
            return;
        }
        races.removeIf(r -> Objects.equals(r.getId(), race.getId()));
    }
}
