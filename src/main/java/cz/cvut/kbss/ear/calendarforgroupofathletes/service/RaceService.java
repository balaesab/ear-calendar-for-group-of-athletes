package cz.cvut.kbss.ear.calendarforgroupofathletes.service;

import cz.cvut.kbss.ear.calendarforgroupofathletes.dao.RaceDao;
import cz.cvut.kbss.ear.calendarforgroupofathletes.dto.Highlighting;
import cz.cvut.kbss.ear.calendarforgroupofathletes.dto.RaceGetDto;
import cz.cvut.kbss.ear.calendarforgroupofathletes.dto.RaceOverviewGetDto;
import cz.cvut.kbss.ear.calendarforgroupofathletes.model.Competition;
import cz.cvut.kbss.ear.calendarforgroupofathletes.model.Participation;
import cz.cvut.kbss.ear.calendarforgroupofathletes.model.Race;
import cz.cvut.kbss.ear.calendarforgroupofathletes.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static cz.cvut.kbss.ear.calendarforgroupofathletes.dto.Highlighting.*;
import static cz.cvut.kbss.ear.calendarforgroupofathletes.security.SecurityUtils.getCurrentUser;

@Service
@Transactional
public class RaceService extends BaseService<Race> {

    @Autowired
    public RaceService(RaceDao dao) {
        super(dao);
    }

    /**
     * Removes race, which consists of setting its {@code isRemoved} attribute to {@code true}.
     *
     * @param race Race to remove
     */
    public void remove(Race race) {
        Objects.requireNonNull(race);
        race.setRemoved(true);
        dao.update(race);
    }

    public String getLocationLink(Race race) {
        StringBuilder sb = new StringBuilder();
        for (String linkPart : race.getLocation().getLinkParts()) {
            sb.append(linkPart);
        }
        return sb.toString();
    }

    public Highlighting getHighlightingForARace(Race race, User requestingUser) {
        if (requestingUser == null) {
            return GENERAL;
        }

        boolean closeFriendParticipates = false;
        boolean userParticipates = false;

        for (Participation participation : race.getParticipations()) {
            User participant = participation.getParticipant();
            if (requestingUser.getCloseFriends().contains(participant)) {
                closeFriendParticipates = true;
                break;
            }
        }

        for (Participation participation : race.getParticipations()) {
            User participant = participation.getParticipant();
            if (requestingUser.equals(participant)) {
                userParticipates = true;
                break;
            }
        }

        return toHighlighting(closeFriendParticipates, userParticipates);
    }

    public Highlighting toHighlighting(boolean closeFriendParticipates, boolean userParticipates) {
        return closeFriendParticipates ?
                userParticipates ? COMMON : FRIENDLY :
                userParticipates ? SOLO : GENERAL;
    }

    public RaceGetDto convertIntoRaceDto(Race race) {
        RaceGetDto raceGetDto = new RaceGetDto();

        raceGetDto.setName(race.getName());
        raceGetDto.setDate(race.getDate());
        raceGetDto.setTime(race.getTime());
        raceGetDto.setStartingFee(race.getStartingFee());
        raceGetDto.setNote(race.getNote());
        raceGetDto.setLocationName(race.getLocation().getName());
        raceGetDto.setLocationLink(this.getLocationLink(race));

        List<User> participatingUsers = mapToCollectList(race.getParticipations().stream(), Participation::getParticipant);

        List<String> participatingUsernames = mapToCollectList(participatingUsers.stream(), User::getUsername);
        List<String> participatingFirstNames = mapToCollectList(participatingUsers.stream(), User::getFirstName);
        List<String> participatingLastNames = mapToCollectList(participatingUsers.stream(), User::getLastName);

        raceGetDto.setParticipatingUsernames(participatingUsernames);
        raceGetDto.setParticipatingFirstNames(participatingFirstNames);
        raceGetDto.setParticipatingLastNames(participatingLastNames);

        raceGetDto.setCompetitionId(race.getCompetition().getId());
        raceGetDto.setCompetitionName(race.getCompetition().getName());

        return raceGetDto;
    }

    public RaceOverviewGetDto convertIntoRaceOverviewDto(List<Race> races, User requestingUser) {
        RaceOverviewGetDto raceOverviewDto = new RaceOverviewGetDto();

        List<Integer> racesIds = mapToCollectList(races.stream(), Race::getId);
        List<String> racesNames = mapToCollectList(races.stream(), Race::getName);
        List<LocalDate> racesDates = mapToCollectList(races.stream(), Race::getDate);
        List<LocalTime> racesTimes = mapToCollectList(races.stream(), Race::getTime);
        List<String> raceLocationsNames = mapToCollectList(races.stream(), race -> race.getLocation().getName());
        /*List<List<String>> racesCloseFriendsParticipantsUsernames = mapToCollectList(races.stream(),
                race -> mapToCollectList(racesToCloseFriends.getOrDefault(race, new ArrayList<>()).stream(),
                        User::getUsername));*/

        List<Competition> competitions = mapToCollectList(races.stream(), Race::getCompetition);

        List<Integer> competitionsIds = mapToCollectList(competitions.stream(), competition -> competition == null ? null : competition.getId());
        List<String> competitionsNames = mapToCollectList(competitions.stream(), competition -> competition == null ? null : competition.getName());

        List<Highlighting> highlightingValues = races.stream().map(race -> getHighlightingForARace(race, getCurrentUser())).collect(Collectors.toCollection(ArrayList::new));

        raceOverviewDto.setRacesIds(racesIds);
        raceOverviewDto.setRacesNames(racesNames);
        raceOverviewDto.setRacesDates(racesDates);
        raceOverviewDto.setRacesTimes(racesTimes);
        raceOverviewDto.setRaceLocationsNames(raceLocationsNames);
        //raceOverviewDto.setRacesCloseFriendsParticipantsUsernames(racesCloseFriendsParticipantsUsernames);
        raceOverviewDto.setCompetitionsIds(competitionsIds);
        raceOverviewDto.setCompetitionsNames(competitionsNames);
        raceOverviewDto.setHighlightingValues(highlightingValues);

        return raceOverviewDto;
    }

    public Race convertIntoRace(RaceGetDto raceDto) {
        Race race = new Race();

        race.setName(raceDto.getName());
        //race.setCompetition(raceDto.getCompetitionName());
        race.setDate(raceDto.getDate());
        race.setTime(raceDto.getTime());
        //race.setLocation(raceDto.getLocationName());
        race.setStartingFee(raceDto.getStartingFee());
        race.setNote(raceDto.getNote());

        return race;
    }
}
