package cz.cvut.kbss.ear.calendarforgroupofathletes.dao;

import cz.cvut.kbss.ear.calendarforgroupofathletes.model.Group;
import cz.cvut.kbss.ear.calendarforgroupofathletes.model.Participation;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;

@Repository
public class ParticipationDao extends BaseDao<Participation> {
    protected ParticipationDao() {
        super(Participation.class);
    }

    public Participation findByRaceAndParticipantId(Integer raceId, Integer participantId) {
        try {
            return em.createNamedQuery("Participation.findByRaceAndParticipant", Participation.class)
                    .setParameter("raceId", raceId)
                    .setParameter("participantId", participantId)
                    .getSingleResult();

        } catch (NoResultException e) {
            return null;
        }
    }
}
