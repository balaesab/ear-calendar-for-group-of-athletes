package cz.cvut.kbss.ear.calendarforgroupofathletes.model;

public enum Role {
    ATHLETE("ROLE_ATHLETE"), COACH("ROLE_COACH");

    private final String name;

    Role(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
