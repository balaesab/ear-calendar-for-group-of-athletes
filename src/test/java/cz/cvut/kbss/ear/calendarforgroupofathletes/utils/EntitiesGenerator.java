package cz.cvut.kbss.ear.calendarforgroupofathletes.utils;

import cz.cvut.kbss.ear.calendarforgroupofathletes.model.Race;
import cz.cvut.kbss.ear.calendarforgroupofathletes.model.Role;
import cz.cvut.kbss.ear.calendarforgroupofathletes.model.User;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class EntitiesGenerator {

    public static User generateUser() {
        final User user = new User();
        user.setFirstName("FirstName" + RandomGenerator.randomInt());
        user.setLastName("LastName" + RandomGenerator.randomInt());
        user.setUsername("username" + RandomGenerator.randomInt());
        user.setPassword(Integer.toString(RandomGenerator.randomInt()));
        user.setRole(generateRole());
        user.setGroups(new ArrayList<>());
        user.setCloseFriends(new ArrayList<>());
        return user;
    }

    public static Role generateRole() {
        return Role.values()[RandomGenerator.randomInt(Role.values().length)];
    }

    public static Race generateRace() {
        final Race race = new Race();
        race.setName("Name" + RandomGenerator.randomInt());
        race.setDate(LocalDate.of(
                RandomGenerator.randomInt(30) + 2000,
                RandomGenerator.randomInt(12) + 1,
                RandomGenerator.randomInt(28) + 1));
        race.setTime(LocalTime.of(
                RandomGenerator.randomInt(24),
                RandomGenerator.randomInt(60),
                RandomGenerator.randomInt(60),
                RandomGenerator.randomInt(1000000000)));
        race.setNote("Note" + RandomGenerator.randomInt());
        race.setStartingFee(RandomGenerator.randomInt());
        race.setParticipations(new ArrayList<>());
        race.setId(RandomGenerator.randomInt());
        return race;
    }
}
