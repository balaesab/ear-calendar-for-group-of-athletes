package cz.cvut.kbss.ear.calendarforgroupofathletes.model;

import cz.cvut.kbss.ear.calendarforgroupofathletes.utils.EntitiesGenerator;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class UserTest {

    @Test
    public void addRaceWorksWhenAddingRaceForFirstTime() {
        final User user = EntitiesGenerator.generateUser();
        final Race race = EntitiesGenerator.generateRace();
        final Participation participation = new Participation(user, race);

        user.addParticipation(participation);
        race.addParticipation(participation);

        assertEquals(1, user.getParticipations().size());
        assertEquals(user.getParticipations().get(0), participation);
    }

    @Test
    public void addRaceWorksForUserWithExistingRace() {
        final User user = EntitiesGenerator.generateUser();
        final Race raceOne = EntitiesGenerator.generateRace();
        final Participation participationOne = new Participation(user, raceOne);

        user.setParticipations(new ArrayList<>(Collections.singletonList(participationOne)));

        final Race addedRace = EntitiesGenerator.generateRace();
        final Participation participationTwo = new Participation(user, addedRace);

        user.addParticipation(participationTwo);

        assertEquals(2, user.getParticipations().size());
        assertTrue(
                (user.getParticipations().get(0) == participationOne &&
                        user.getParticipations().get(1) == participationTwo) ||
                        (user.getParticipations().get(0) == participationTwo &&
                                user.getParticipations().get(1) == participationOne)
        );
    }
}
