package cz.cvut.kbss.ear.calendarforgroupofathletes.dao;

import cz.cvut.kbss.ear.calendarforgroupofathletes.exception.PersistenceException;
import cz.cvut.kbss.ear.calendarforgroupofathletes.model.*;
import cz.cvut.kbss.ear.calendarforgroupofathletes.model.User;
import cz.cvut.kbss.ear.calendarforgroupofathletes.utils.EntitiesGenerator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
@ComponentScan(basePackageClasses = cz.cvut.kbss.ear.calendarforgroupofathletes.CalendarForGroupOfAthletesApplication.class)
public class BaseDaoTest {
    @Autowired
    private TestEntityManager em;

    @Autowired
//    private BaseDao<AbstractEntity> sut;
    private UserDao sut;

    @Autowired
    private RaceDao raceSut;

    @Test
    public void persistSavesSpecifiedInstance() {
        final User user = EntitiesGenerator.generateUser();
        sut.persist(user);
        assertNotNull(user.getId());

        final AbstractEntity result = em.find(User.class, user.getId());
        assertNotNull(result);
        assertEquals(user.getId(), result.getId());
    }

    @Test
    public void findRetrievesInstanceByIdentifier() {
        final User user = EntitiesGenerator.generateUser();
        em.persist(user);
        em.flush();
        assertNotNull(user.getId());

        final User result = sut.find(user.getId());
        assertNotNull(result);
        assertEquals(user.getId(), result.getId());
        assertEquals(user.getFirstName(), result.getFirstName());
    }

    @Test
    public void findAllRetrievesAllInstancesOfType() {
        final User user = EntitiesGenerator.generateUser();
        final User user2 = EntitiesGenerator.generateUser();
        em.persist(user);
        em.flush();
        em.persist(user2);
        em.flush();

        final List<User> result = sut.findAll();
        assertEquals(2, result.size());
        assertTrue(result.stream().anyMatch(c -> c.getId().equals(user.getId())));
        assertTrue(result.stream().anyMatch(c -> c.getId().equals(user2.getId())));
    }

    @Test
    public void updateUpdatesExistingInstance() {
        final User user = EntitiesGenerator.generateUser();
        em.persist(user);
        em.flush();

        final User update = new User();
        update.setId(user.getId());
        sut.update(update);

        final AbstractEntity result = sut.find(user.getId());
        assertNotNull(result);
        assertEquals(user.getId(), result.getId());
    }

    @Test
    public void removeRemovesSpecifiedInstance() {
        final Race race = EntitiesGenerator.generateRace();
        em.persist(race);
        em.flush();
        assertNotNull(em.find(Race.class, race.getId()));
        em.detach(race);

        raceSut.remove(race);
        assertNull(em.find(Race.class, race.getId()));
    }

    @Test
    public void removeDoesNothingWhenInstanceDoesNotExist() {
        final User user = EntitiesGenerator.generateUser();
        user.setId(123);
        assertNull(em.find(User.class, user.getId()));

        sut.remove(user);
        assertNull(em.find(User.class, user.getId()));
    }

    @Test
    public void exceptionOnPersistInWrappedInPersistenceException() {
        final User user = EntitiesGenerator.generateUser();
        em.persist(user);
        em.flush();
        em.remove(user);
        assertThrows(PersistenceException.class, () -> sut.update(user));
    }

    @Test
    public void existsReturnsTrueForExistingIdentifier() {
        final User user = EntitiesGenerator.generateUser();
        em.persist(user);
        em.flush();
        assertTrue(sut.exists(user.getId()));
        assertFalse(sut.exists(-1));
    }
}
