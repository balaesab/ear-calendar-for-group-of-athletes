package cz.cvut.kbss.ear.calendarforgroupofathletes.utils;

import cz.cvut.kbss.ear.calendarforgroupofathletes.model.Role;

import java.util.Random;

public class RandomGenerator {

    private static final Random RANDOM = new Random();

    public static int randomInt() {
        return RANDOM.nextInt();
    }
    public static int randomInt(int bound) {
        return RANDOM.nextInt(bound);
    }
}
